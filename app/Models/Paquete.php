<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Paquete
 *
 * @property int $id
 * @property int|null $numero
 * @property string|null $solicitud_afin
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Collection|Solicitud[] $solicitudes
 *
 * @package App\Models
 */
class Paquete extends Model
{
	protected $table = 'paquetes';

	protected $casts = [
		'numero' => 'int'
	];

	protected $fillable = [
		'numero',
		'solicitud_afin'
	];

	public function solicitudes()
	{
		return $this->belongsToMany(Solicitud::class, 'paquete_solicitudes', 'id_paquete', 'id_solicitud')
					->withPivot('id')
					->withTimestamps();
	}
}
