<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ResponsableDependencia
 * 
 * @property int|null $id_user
 * @property int|null $id_dependencia
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $id
 *
 * @package App\Models
 */
class ResponsableDependencia extends Model
{
	protected $table = 'responsable_dependencias';

	protected $casts = [
		'id_user' => 'int',
		'id_dependencia' => 'int'
	];

	protected $fillable = [
		'id_user',
		'id_dependencia'
	];
}
