<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ComplementoPago
 *
 * @property int $id
 * @property int|null $id_solicitud
 * @property Carbon|null $fecha_pago
 * @property string|null $observaciones
 * @property string|null $factura_ingreso
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Solicitud|null $solicitude
 *
 * @package App\Models
 */
class ComplementoPago extends Model
{
	protected $table = 'complemento_pagos';

	protected $casts = [
		'id_solicitud' => 'int'
	];

	protected $dates = [
		'fecha_pago'
	];

	protected $fillable = [
		'id_solicitud',
		'fecha_pago',
		'observaciones',
		'factura_ingreso'
	];

	public function solicitude()
	{
		return $this->belongsTo(Solicitud::class, 'id_solicitud');
	}
}
