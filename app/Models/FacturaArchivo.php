<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FacturaArchivo
 *
 * @property int $id
 * @property string|null $nombre
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|null $id_solicitud
 *
 * @property Solicitud|null $solicitude
 *
 * @package App\Models
 */
class FacturaArchivo extends Model
{
	protected $table = 'factura_archivos';

	protected $casts = [
		'id_solicitud' => 'int'
	];

	protected $fillable = [
		'nombre',
		'id_solicitud'
	];

	public function solicitud()
	{
		return $this->belongsTo(Solicitud::class, 'id_solicitud');
	}
}
