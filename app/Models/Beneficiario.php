<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Beneficiario
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $cuenta
 * @property int|null $id_dependencia
 * @property int|null $codigo
 * @property string|null $observaciones
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Dependencia|null $dependencia
 * @property Collection|Solicitud[] $solicitudes
 *
 * @package App\Models
 */
class Beneficiario extends Model
{
	protected $table = 'beneficiarios';

	protected $casts = [
		'id_dependencia' => 'int',
		'codigo' => 'int'
	];

	protected $fillable = [
		'nombre',
		'cuenta',
		'id_dependencia',
		'codigo',
		'observaciones'
	];

	public function dependencia()
	{
		return $this->belongsTo(Dependencia::class, 'id_dependencia');
	}

	public function solicitudes()
	{
		return $this->hasMany(Solicitud::class, 'id_beneficiario');
	}
}
