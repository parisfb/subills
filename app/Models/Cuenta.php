<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cuenta
 * 
 * @property int $id
 * @property string|null $num_cuenta
 * @property int|null $id_banco
 * @property string|null $clabe
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 * 
 * @property Banco|null $banco
 *
 * @package App\Models
 */
class Cuenta extends Model
{
	protected $table = 'cuentas';

	protected $casts = [
		'id_banco' => 'int'
	];

	protected $fillable = [
		'num_cuenta',
		'id_banco',
		'clabe'
	];

	public function banco()
	{
		return $this->belongsTo(Banco::class, 'id_banco');
	}
}
