<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Compensacione
 *
 * @property int $id
 * @property int|null $id_status
 * @property Carbon|null $fecha
 * @property int|null $folio
 * @property int|null $destino
 * @property string|null $justificacion
 * @property float|null $monto
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Status|null $status
 * @property CompensacionesOrigen $compensaciones_origen
 *
 * @package App\Models
 */
class Compensaciones extends Model
{
	protected $table = 'compensaciones';

	protected $casts = [
		'id_status' => 'int',
		'folio' => 'int',
		'destino' => 'int',
		'monto' => 'float'
	];

	protected $dates = [
		'fecha'
	];

	protected $fillable = [
		'id_status',
		'fecha',
		'folio',
		'destino',
		'justificacion',
		'monto'
	];

	public function status()
	{
		return $this->belongsTo(Status::class, 'id_status');
	}

	public function compensaciones_origen()
	{
		return $this->hasOne(CompensacionesOrigen::class, 'id_compensacion');
	}
}
