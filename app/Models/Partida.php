<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Partida
 *
 * @property int $id
 * @property int|null $id_proyecto
 * @property int|null $numero
 * @property float|null $monto
 * @property string|null $descripcion
 * @property int|null $cog
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Proyecto|null $proyecto
 * @property CompensacionesOrigen $compensaciones_origen
 * @property Collection|SaldoPartida[] $saldos_partidas
 *
 * @package App\Models
 */
class Partida extends Model
{
	protected $table = 'partidas';

	protected $casts = [
		'id_proyecto' => 'int',
		'numero' => 'int',
		'monto' => 'float',
		'cog' => 'int'
	];

	protected $fillable = [
		'id_proyecto',
		'numero',
		'monto',
		'descripcion',
		'cog'
	];

	public function proyecto()
	{
		return $this->belongsTo(Proyecto::class, 'id_proyecto', 'id');
	}

	public function compensaciones_origen()
	{
		return $this->hasOne(CompensacionesOrigen::class, 'id_partida');
	}

	public function saldos_partidas()
	{
		return $this->hasMany(SaldoPartida::class, 'id_partida');
	}
}
