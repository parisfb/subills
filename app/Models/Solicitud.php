<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Solicitude
 *
 * @property int $id
 * @property int|null $id_status
 * @property int|null $id_proyecto
 * @property Carbon|null $fecha
 * @property int|null $folio
 * @property string|null $concepto
 * @property int|null $id_beneficiario
 * @property int|null $tipo
 * @property int|null $tipo_afin
 * @property int|null $id_fondo
 * @property float|null $monto
 * @property float|null $reintegro
 * @property string|null $orden_compra
 * @property int|null $solicitud_afin
 * @property int|null $aprobacion_afin
 * @property int|null $orden_compra_afin
 * @property int|null $recepcion_afin
 * @property int|null $factura_afin
 * @property string|null $movimiento
 * @property Carbon|null $fecha_pago
 * @property int|null $paquete_finanzas
 * @property Carbon|null $recibido_finanzas
 * @property int|null $id_incidencia
 * @property string|null $observaciones
 * @property int|null $id_usuario_solicitante
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Beneficiario|null $beneficiario
 * @property Fondo|null $fondo
 * @property Incidencia|null $incidencia
 * @property Proyecto|null $proyecto
 * @property Status|null $status
 * @property User|null $user
 * @property Collection|Anexo[] $anexos
 * @property Collection|ComplementoPagoArchivo[] $complemento_pago_archivos
 * @property Collection|ComplementoPago[] $complemento_pagos
 * @property Collection|FacturaArchivo[] $factura_archivos
 * @property Collection|Factura[] $facturas
 * @property Collection|Movimiento[] $movimientos
 * @property Collection|Paquete[] $paquetes
 * @property Collection|Reintegro[] $reintegros
 *
 * @package App\Models
 */
class Solicitud extends Model
{
	protected $table = 'solicitudes';

	protected $casts = [
		'id_status' => 'int',
		'id_proyecto' => 'int',
		'folio' => 'int',
		'id_beneficiario' => 'int',
		'tipo' => 'int',
		'tipo_afin' => 'int',
		'id_fondo' => 'int',
		'monto' => 'float',
		'reintegro' => 'float',
		'solicitud_afin' => 'int',
		'aprobacion_afin' => 'int',
		'orden_compra_afin' => 'int',
		'recepcion_afin' => 'int',
		'factura_afin' => 'int',
		'paquete_finanzas' => 'int',
		'id_incidencia' => 'int',
		'id_usuario_solicitante' => 'int'
	];

	protected $dates = [
		'fecha',
		'fecha_pago',
		'recibido_finanzas'
	];

	protected $fillable = [
		'id_status',
		'id_proyecto',
		'fecha',
		'folio',
		'concepto',
		'id_beneficiario',
		'tipo',
		'tipo_afin',
		'id_fondo',
		'monto',
		'reintegro',
		'orden_compra',
		'solicitud_afin',
		'aprobacion_afin',
		'orden_compra_afin',
		'recepcion_afin',
		'factura_afin',
		'movimiento',
		'fecha_pago',
		'paquete_finanzas',
		'recibido_finanzas',
		'id_incidencia',
		'observaciones',
		'id_usuario_solicitante'
	];

	public function beneficiario()
	{
		return $this->belongsTo(Beneficiario::class, 'id_beneficiario');
	}

	public function fondo()
	{
		return $this->belongsTo(Fondo::class, 'id_fondo');
	}

	public function incidencia()
	{
		return $this->belongsTo(Incidencia::class, 'id_incidencia');
	}

	public function proyecto()
	{
		return $this->belongsTo(Proyecto::class, 'id_proyecto');
	}

	public function status()
	{
		return $this->belongsTo(Status::class, 'id_status');
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'id_usuario_solicitante');
	}

	public function anexos()
	{
		return $this->hasMany(Anexo::class, 'id_solicitud');
	}

	public function complemento_pago_archivos()
	{
		return $this->hasMany(ComplementoPagoArchivo::class, 'id_solicitud');
	}

	public function complemento_pagos()
	{
		return $this->hasMany(ComplementoPago::class, 'id_solicitud');
	}

	public function factura_archivos()
	{
		return $this->hasMany(FacturaArchivo::class, 'id_solicitud');
	}

	public function facturas()
	{
		return $this->hasMany(Factura::class, 'id_solicitud');
	}

	public function movimientos()
	{
		return $this->hasMany(Movimiento::class, 'id_solicitud');
	}

	public function paquetes()
	{
		return $this->belongsToMany(Paquete::class, 'paquete_solicitudes', 'id_solicitud', 'id_paquete')
					->withPivot('id')
					->withTimestamps();
	}

	public function reintegros()
	{
		return $this->hasMany(Reintegro::class, 'id_solicitud');
	}
}
