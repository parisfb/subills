<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Status
 *
 * @property int $id
 * @property string|null $concepto
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Collection|Compensaciones[] $compensaciones
 * @property Collection|Factura[] $facturas
 * @property Collection|Solicitud[] $solicitudes
 *
 * @package App\Models
 */
class Status extends Model
{
	protected $table = 'status';

	protected $fillable = [
		'concepto'
	];

	public function compensaciones()
	{
		return $this->hasMany(Compensaciones::class, 'id_status');
	}

	public function facturas()
	{
		return $this->hasMany(Factura::class, 'id_status');
	}

	public function solicitudes()
	{
		return $this->hasMany(Solicitud::class, 'id_status');
	}
}
