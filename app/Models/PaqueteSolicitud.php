<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PaqueteSolicitude
 *
 * @property int $id
 * @property int|null $id_paquete
 * @property int|null $id_solicitud
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Paquete|null $paquete
 * @property Solicitud|null $solicitud
 *
 * @package App\Models
 */
class PaqueteSolicitud extends Model
{
	protected $table = 'paquete_solicitudes';

	protected $casts = [
		'id_paquete' => 'int',
		'id_solicitud' => 'int'
	];

	protected $fillable = [
		'id_paquete',
		'id_solicitud'
	];

	public function paquete()
	{
		return $this->belongsTo(Paquete::class, 'id_paquete');
	}

	public function solicitud()
	{
		return $this->belongsTo(Solicitud::class, 'id_solicitud');
	}
}
