<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Factura
 *
 * @property int $id
 * @property int|null $id_solicitud
 * @property string|null $folio_fiscal
 * @property int|null $id_metodo
 * @property int|null $id_forma_pago
 * @property int|null $id_status
 * @property string|null $emisor
 * @property float|null $total
 * @property string|null $observaciones
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property FormaPago|null $foma_pago
 * @property Metodo|null $metodo
 * @property Solicitud|null $solicitude
 * @property Status|null $status
 *
 * @package App\Models
 */
class Factura extends Model
{
	protected $table = 'facturas';

	protected $casts = [
		'id_solicitud' => 'int',
		'id_metodo' => 'int',
		'id_forma_pago' => 'int',
		'id_status' => 'int',
		'total' => 'float'
	];

	protected $fillable = [
		'id_solicitud',
		'folio_fiscal',
		'id_metodo',
		'id_forma_pago',
		'id_status',
		'emisor',
		'total',
		'observaciones'
	];

	public function forma_pago()
	{
		return $this->belongsTo(FormaPago::class, 'id_forma_pago');
	}

	public function metodo()
	{
		return $this->belongsTo(Metodo::class, 'id_metodo');
	}

	public function solicitud()
	{
		return $this->belongsTo(Solicitud::class, 'id_solicitud');
	}

	public function status()
	{
		return $this->belongsTo(Status::class, 'id_status');
	}
}
