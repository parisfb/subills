<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Banco
 *
 * @property int $id
 * @property string|null $nombre
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Collection|Cuenta[] $cuentas
 * @property Collection|Proveedor[] $provedores
 *
 * @package App\Models
 */
class Banco extends Model
{
	protected $table = 'bancos';

	protected $fillable = [
		'nombre'
	];

	public function cuentas()
	{
		return $this->hasMany(Cuenta::class, 'id_banco');
	}

	public function provedores()
	{
		return $this->hasMany(Proveedor::class, 'id_banco');
	}
}
