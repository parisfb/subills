<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Movimiento
 *
 * @property int $id
 * @property Carbon|null $fecha
 * @property string|null $descripcion
 * @property float|null $monto
 * @property string|null $tipo
 * @property string|null $folio
 * @property int|null $id_solicitud
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Solicitud|null $solicitude
 *
 * @package App\Models
 */
class Movimiento extends Model
{
	protected $table = 'movimientos';

	protected $casts = [
		'monto' => 'float',
		'id_solicitud' => 'int'
	];

	protected $dates = [
		'fecha'
	];

	protected $fillable = [
		'fecha',
		'descripcion',
		'monto',
		'tipo',
		'folio',
		'id_solicitud'
	];

	public function solicitude()
	{
		return $this->belongsTo(Solicitud::class, 'id_solicitud');
	}
}
