<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FomaPago
 *
 * @property int $id
 * @property string|null $nombre
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Factura[] $facturas
 *
 * @package App\Models
 */
class FormaPago extends Model
{
	protected $table = 'forma_pagos';

	protected $fillable = [
		'nombre'
	];

	public function facturas()
	{
		return $this->hasMany(Factura::class, 'id_forma_pago');
	}
}
