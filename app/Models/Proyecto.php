<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Proyecto
 *
 * @property int $id
 * @property int|null $numero
 * @property string|null $nombre
 * @property int|null $id_fondo
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Fondo|null $fondo
 * @property Collection|Partida[] $partidas
 * @property Collection|SaldoProyecto[] $saldo_proyectos
 * @property Collection|Solicitud[] $solicitudes
 *
 * @package App\Models
 */
class Proyecto extends Model
{
	protected $table = 'proyectos';

	protected $casts = [
		'numero' => 'int',
		'id_fondo' => 'int'
	];

	protected $fillable = [
		'numero',
		'nombre',
		'id_fondo'
	];

	public function fondo()
	{
		return $this->belongsTo(Fondo::class, 'id_fondo');
	}

	public function partidas()
	{
		return $this->hasMany(Partida::class, 'id_proyecto', 'id_fondo');
	}

	public function saldo_proyectos()
	{
		return $this->hasMany(SaldoProyecto::class, 'id_proyecto');
	}

	public function solicitudes()
	{
		return $this->hasMany(Solicitud::class, 'id_proyecto');
	}
}
