<?php

namespace App\Http\Controllers;

use App\Models\ComplementoPago;
use App\Models\Solicitud;
use Illuminate\Http\Request;

class ComplementoPagosController extends Controller
{
    public function index()
    {
        $complemento_pagos = ComplementoPago::with('solicitude')->get();
        $data = [
            'complemento_pagos' => $complemento_pagos
        ];
        return view('complemento_pagos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $complemento_pago = new ComplementoPago();
        $complemento_pago->id_solicitud = '';
        $complemento_pago->fecha_pago = '';
        $complemento_pago->factura_ingreso = '';
        $complemento_pago->observaciones = '';
        $solicitudes = Solicitud::all();
        $data = [
            'complemento_pago' => $complemento_pago,
            'create' => true,
            'solicitudes' => $solicitudes,
        ];
        return view('complemento_pagos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $complemento_pago = new ComplementoPago();
        $complemento_pago->id_solicitud = $request->id_solicitud;
        $complemento_pago->fecha_pago = $request->fecha_pago;
        $complemento_pago->factura_ingreso = $request->factura_ingreso;
        $complemento_pago->observaciones = $request->observaciones;
        try {
            $complemento_pago->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComplementoPago  $complemento_pago
     */
    public function show($id)
    {
        $complemento_pago = ComplementoPago::with('solicitude')->find($id);
        $data = [
            'complemento_pago' => $complemento_pago
        ];
        return view('complemento_pagos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComplementoPago  $complemento_pago
     */
    public function edit($id)
    {
        $complemento_pago = ComplementoPago::find($id);
        $solicitudes = Solicitud::all();
        $data = [
            'complemento_pago' => $complemento_pago,
            'create' => 'false',
            'solicitudes' => $solicitudes
        ];
        return view('complemento_pagos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $complemento_pago = ComplementoPago::find($request->id);
        $complemento_pago->id_solicitud = $request->id_solicitud;
        $complemento_pago->fecha_pago = $request->fecha_pago;
        $complemento_pago->factura_ingreso = $request->factura_ingreso;
        $complemento_pago->observaciones = $request->observaciones;
        try {
            $complemento_pago->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            dd($e);
            $information = 'bad';
        }
        return ['information' => $information];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComplementoPago  $complemento_pago
     */
    public function destroy($id)
    {
        $complemento_pago = ComplementoPago::find($id);
        try {
            $complemento_pago->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        return ['information' => $information];
    }
}
