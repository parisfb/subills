<?php

namespace App\Http\Controllers;

use App\Models\Compensaciones;
use App\Models\CompensacionesOrigen;
use App\Models\Partida;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;

class CompensacionesOrigenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $compensaciones_origen = CompensacionesOrigen::with('partida', 'compensaciones')->get();
        $data = [
            'compensaciones_origen' => $compensaciones_origen,
        ];
        return view('compensaciones_origen.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $compensacion_origen = new CompensacionesOrigen();
        $compensacion_origen-> id_compensacion = 0;
        $compensacion_origen-> id_partida = 0;
        $compensaciones = Compensaciones::all();
        $partidas = Partida::all();
        $data = [
            'compensaciones' => $compensaciones,
            'partidas' => $partidas,
            'create' => true,
            "compensacion_origen" => $compensacion_origen,
        ];
        return view('compensaciones_origen.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $compensacion_origen = new CompensacionesOrigen();
        $compensacion_origen->id_partida = $request ->id_partida;
        $compensacion_origen->id_compensacion = $request -> id_compensacion;
        try {
            $compensacion_origen->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $compensacion_origen = CompensacionesOrigen::with('compensaciones', 'partida')->find($id);
        $data = [
            'compensacion_origen' => $compensacion_origen,
        ];
        return view('compensaciones_origen.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($id)
    {
        $compensacion_origen = CompensacionesOrigen::find($id);
        $compensaciones = Compensaciones::all();
        $partidas = Partida::all();
        $data = [
            'compensaciones' => $compensaciones,
            'partidas' => $partidas,
            'create' => 'false',
            "compensacion_origen" => $compensacion_origen,
        ];
        return view('compensaciones_origen.create-add', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $compensacion_origen = CompensacionesOrigen::find($request->id);
        $compensacion_origen->id_partida = $request ->id_partida;
        $compensacion_origen->id_compensacion = $request -> id_compensacion;
        try {
            $compensacion_origen->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $compensacionorigen = CompensacionesOrigen::find($id);
        try {
            $compensacionorigen->delete();
            $information = 'good';
        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}
