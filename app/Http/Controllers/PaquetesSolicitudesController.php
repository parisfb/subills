<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use App\Models\PaqueteSolicitud;
use App\Models\Solicitud;
use Illuminate\Http\Request;

class PaquetesSolicitudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $paquetes_solicitudes = PaqueteSolicitud::with('solicitud', 'paquete')->get();
        $data = [
            'paquetes_solicitudes' => $paquetes_solicitudes,
        ];
        return view('paquete_solicitud.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $paquete_solicitud = new PaqueteSolicitud();
        $paquete_solicitud-> id_paquete = 0;
        $paquete_solicitud-> id_solicitud = 0;
        $paquetes = Paquete::all();
        $solicitudes = Solicitud::all();
        $data = [
            'paquetes' => $paquetes,
            'solicitudes' => $solicitudes,
            'create' => true,
            "paquete_solicitud" => $paquete_solicitud,
        ];
        return view('paquete_solicitud.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $paquete_solicitud = new PaqueteSolicitud();
        $paquete_solicitud-> id_solicitud = $request->id_solicitud;
        $paquete_solicitud-> id_paquete = $request->id_paquete;
        try{
            $paquete_solicitud->save();
            $information = 'good';
        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
            dd($e);
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaqueteSolicitud  $paqueteSolicitud
     */
    public function show($id)
    {
        $paquete_solicitud = PaqueteSolicitud::with('solicitud', 'paquete')->find($id);
        $data = [
            'paquete_solicitud' => $paquete_solicitud,
        ];
        return view('paquete_solicitud.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($id)
    {
        $paquete_solicitud = PaqueteSolicitud::find($id);
        $paquetes = Paquete::all();
        $solicitudes = Solicitud::all();
        $data = [
            'paquete_solicitud' => $paquete_solicitud,
            'paquetes' => $paquetes,
            'solicitudes' => $solicitudes,
            'create' => 'false',
        ];
        return view('paquete_solicitud.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $paquete_solicitud = PaqueteSolicitud::find($request->id);
        $paquete_solicitud-> id_paquete = $request->id_paquete;
        $paquete_solicitud-> id_solicitud = $request->id_solicitud;
        try{
            $paquete_solicitud->save();
            $information = 'good';
        }catch (\Illuminate\Database\QueryException $e){
            dd($e);
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $paquete_solicitud = PaqueteSolicitud::find($id);
        try{
            $paquete_solicitud->delete();
            $information = 'good';
        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }
}
