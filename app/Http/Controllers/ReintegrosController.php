<?php

namespace App\Http\Controllers;

use App\Models\Reintegro;
use App\Models\Solicitud;
use Illuminate\Http\Request;

class ReintegrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $reintegros = Reintegro::with('solicitude')->get();
        $data = [
            'reintegros' => $reintegros
        ];
        return view('reintegros.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $reintegro = new Reintegro();
        $reintegro->folio = '';
        $reintegro->id_solicitud = '';
        $reintegro->fecha = '';
        $reintegro->concepto = '';
        $reintegro->referencia = '';
        $reintegro->monto = '';
        $reintegro->observaciones = '';
        $solicitudes = Solicitud::all();
        $data = [
            'reintegro' => $reintegro,
            'create' => true,
            'solicitudes' => $solicitudes,
        ];
        return view('reintegros.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $reintegro = new Reintegro();
        $reintegro->folio = $request->folio;
        $reintegro->id_solicitud = $request->id_solicitud;
        $reintegro->fecha = $request->fecha;
        $reintegro->concepto = $request->concepto;
        $reintegro->referencia = $request->referencia;
        $reintegro->monto = $request->monto;
        $reintegro->observaciones = $request->observaciones;
        try {
            $reintegro->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reintegro  $reintegro
     */
    public function show($id)
    {
        $reintegro = Reintegro::with('solicitude')->find($id);
        $data = [
            'reintegro' => $reintegro
        ];
        return view('reintegros.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reintegro  $reintegro
     */
    public function edit($id)
    {
        $reintegro = Reintegro::find($id);
        $solicitudes = Solicitud::all();
        $data = [
            'reintegro' => $reintegro,
            'create' => 'false',
            'solicitudes' => $solicitudes
        ];
        return view('reintegros.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $reintegro = Reintegro::find($request->id);
        $reintegro->folio = $request->folio;
        $reintegro->id_solicitud = $request->id_solicitud;
        $reintegro->fecha = $request->fecha;
        $reintegro->concepto = $request->concepto;
        $reintegro->referencia = $request->referencia;
        $reintegro->monto = $request->monto;
        $reintegro->observaciones = $request->observaciones;
        try {
            $reintegro->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            dd($e);
            $information = 'bad';
        }
        return ['information' => $information];
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $reintegro = Reintegro::find($id);
        try {
            $reintegro->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        return ['information' => $information];
    }
}
