<?php

namespace App\Http\Controllers;

use App\Models\Fondo;
use App\Models\Partida;
use App\Models\SaldoPartida;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SaldoPartidasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $saldo_partidas = SaldoPartida::with(['partida', 'fondo'])->get();
        $data = [
            'saldo_partidas' => $saldo_partidas
        ];
        return view('saldo_partidas.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $partidas = Partida::all();
        $fondos = Fondo::all();
        $saldo_partida = new SaldoPartida();
        $saldo_partida->id_fondo = 0;
        $saldo_partida->id_partida = 0;
        $saldo_partida->adeudo = 0.0;
        $saldo_partida->capturado = 0.0;
        $saldo_partida->compensado = 0.0;
        $saldo_partida->inicial = 0.0;
        $saldo_partida->pagado = 0.0;
        $saldo_partida->porcentaje_compensado = 0.0;
        $saldo_partida->saldo = 0.0;
        $saldo_partida->solicitado = 0.0;

        $data = [
            'saldo_partida' => $saldo_partida,
            'partidas' => $partidas,
            'fondos' => $fondos,
            'create' => true,
        ];

        return view('saldo_partidas.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(Request $request)
    {
        $saldo_partida = new SaldoPartida();
        $saldo_partida->id_fondo = $request-> id_fondo;
        $saldo_partida->id_partida = $request-> id_partida;
        $saldo_partida->adeudo = $request-> adeudo;
        $saldo_partida->capturado = $request-> capturado;
        $saldo_partida->compensado = $request-> compensado;
        $saldo_partida->inicial = $request-> inicial;
        $saldo_partida->pagado = $request-> pagado;
        $saldo_partida->porcentaje_compensado = $request-> porcentaje_compensado;
        $saldo_partida->saldo = $request-> saldo;
        $saldo_partida->solicitado = $request-> solicitado;
        try {
            $saldo_partida->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
            dd($e);
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $saldo_partida = SaldoPartida::with(['partida', 'fondo'])->find($id);
        $data = [
            'saldo_partida' => $saldo_partida
        ];
        return view('saldo_partidas.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $partidas = Partida::all();
        $fondos = Fondo::all();
        $saldo_partida = SaldoPartida::find($id);

        $data = [
            'saldo_partida' => $saldo_partida,
            'partidas' => $partidas,
            'fondos' => $fondos,
            'create' => 'false',
        ];

        return view('saldo_partidas.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $saldo_partida = SaldoPartida::find($request->id);
        $saldo_partida->id_fondo = $request-> id_fondo;
        $saldo_partida->id_partida = $request-> id_partida;
        $saldo_partida->adeudo = $request-> adeudo;
        $saldo_partida->capturado = $request-> capturado;
        $saldo_partida->compensado = $request-> compensado;
        $saldo_partida->inicial = $request-> inicial;
        $saldo_partida->pagado = $request-> pagado;
        $saldo_partida->porcentaje_compensado = $request-> porcentaje_compensado;
        $saldo_partida->saldo = $request-> saldo;
        $saldo_partida->solicitado = $request-> solicitado;
        try {
            $saldo_partida->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $saldo_partida = SaldoPartida::find($id);
        try {
            $saldo_partida->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}
