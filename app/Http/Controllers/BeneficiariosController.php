<?php

namespace App\Http\Controllers;

use App\Models\Beneficiario;
use Illuminate\Http\Request;
use App\Models\Dependencia;

class BeneficiariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beneficiarios = Beneficiario::with('dependencia')->get();
        $data = [
            'beneficiarios' => $beneficiarios
        ];
        return view('beneficiarios.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $beneficiario = new Beneficiario();
        $beneficiario->nombre = '';
        $beneficiario->cuenta = '';
        $beneficiario->codigo = '';
        $beneficiario->observaciones = '';
        $beneficiario->id_dependencia = '';
        $dependencias = Dependencia::all();
        $data = [
            'beneficiario' => $beneficiario,
            'create' => true,
            'dependencias' => $dependencias,
        ];
        return view('beneficiarios.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $beneficiario = new Beneficiario();
        $beneficiario->nombre = $request->nombre;
        $beneficiario->cuenta = $request->cuenta;
        $beneficiario->codigo = $request->codigo;
        $beneficiario->observaciones = $request->observaciones;
        $beneficiario->id_dependencia = $request->id_dependencia;
        try {
            $beneficiario->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Beneficiario  $beneficiario
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $beneficiario = Beneficiario::with('dependencia')->find($id);
        $data = [
            'beneficiario' => $beneficiario
        ];
        return view('beneficiarios.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Beneficiario  $beneficiario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $beneficiario = Beneficiario::find($id);
        $dependencias = Dependencia::all();
        $data = [
            'beneficiario' => $beneficiario,
            'create' => 'false',
            'dependencias' => $dependencias
        ];
        return view('beneficiarios.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Beneficiario  $beneficiario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Beneficiario $beneficiario)
    {
        $beneficiario = Beneficiario::find($request->id);
        $beneficiario->nombre = $request->nombre;
        $beneficiario->cuenta = $request->cuenta;
        $beneficiario->codigo = $request->codigo;
        $beneficiario->observaciones = $request->observaciones;
        $beneficiario->id_dependencia = $request->id_dependencia;
        try {
            $beneficiario->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Beneficiario  $beneficiario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $beneficiario = Beneficiario::find($id);
        try {
            $beneficiario->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }
}
