<?php

namespace App\Http\Controllers;

use App\Models\Dependencia;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class DependenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dependencias = Dependencia::all();
        $data = [
          'dependencias' => $dependencias
        ];
        return view('dependencias.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dependencia = new Dependencia();
        $dependencia ->abreviatura = '';
        $dependencia ->nombre = '';
        $data = [
            'dependencia' => $dependencia,
            'create' => true,
        ];
        return view('dependencias.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dependencia = new Dependencia();
        $dependencia->nombre = $request -> nombre;
        $dependencia->abreviatura = $request -> abreviatura;
        try {
            $dependencia->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dependencia  $dependencia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dependencia = Dependencia::find($id);
        $data = [
            'dependencia' => $dependencia
        ];
        return view('dependencias.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dependencia  $dependencia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dependencia = Dependencia::find($id);
        $data = [
            'dependencia' => $dependencia,
            'create' => 'false',
        ];
        return view('dependencias.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dependencia  $dependencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dependencia $dependencia)
    {
        $dependencia = Dependencia::find($request->id);
        $dependencia -> nombre = $request->nombre;
        $dependencia -> abreviatura = $request->abreviatura;
        try {
            $dependencia->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dependencia  $dependencia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dependencia = Dependencia::find($id);
        try {
            $dependencia->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}
