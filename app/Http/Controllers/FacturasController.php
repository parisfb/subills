<?php

namespace App\Http\Controllers;

use App\Models\Factura;
use App\Models\FormaPago;
use App\Models\Metodo;
use App\Models\Solicitud;
use App\Models\Status;
use Illuminate\Http\Request;

class FacturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $facturas = Factura::with('status', 'forma_pago', 'metodo', 'solicitud')->get();
        $data = [
            'facturas' => $facturas
        ];
        return view('facturas.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $factura = new Factura();
        $factura->id_solicitud = '';
        $factura->id_status = '';
        $factura->id_forma_pago = '';
        $factura->id_metodo = '';
        $factura->emisor = '';
        $factura->total = 0.0;
        $factura->observaciones = '';
        $factura->folio_fiscal = '';
        $status = Status::all();
        $solicitudes = Solicitud::all();
        $forma_pagos = FormaPago::all();
        $metodos = Metodo::all();
        $data = [
            'factura' => $factura,
            'create' => true,
            'status' => $status,
            'solicitudes' => $solicitudes,
            'forma_pagos' => $forma_pagos,
            'metodos' => $metodos,
        ];
        return view('facturas.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $factura = new Factura();
        $factura->id_solicitud = $request->id_solicitud;
        $factura->id_status = $request->id_status;
        $factura->id_forma_pago = $request->id_forma_pago;
        $factura->id_metodo = $request->id_metodo;
        $factura->emisor = $request->emisor;
        $factura->total = $request->total;
        $factura->observaciones = $request->observaciones;
        $factura->folio_fiscal = $request->folio_fiscal;
        try {
            $factura ->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        return ['information' => $information];

    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $factura = Factura::with('status', 'forma_pago', 'metodo', 'solicitud')->find($id);
        $data = [
            'factura' => $factura
        ];
        return view('facturas.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($id)
    {
        $factura = Factura::find($id);
        $status = Status::all();
        $solicitudes = Solicitud::all();
        $forma_pagos = FormaPago::all();
        $metodos = Metodo::all();
        $data = [
            'factura' => $factura,
            'create' => 'false',
            'status' => $status,
            'solicitudes' => $solicitudes,
            'forma_pagos' => $forma_pagos,
            'metodos' => $metodos,
        ];
        return view('facturas.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $factura = Factura::find($request->id);
        $factura->id_solicitud = $request->id_solicitud;
        $factura->id_status = $request->id_status;
        $factura->id_forma_pago = $request->id_forma_pago;
        $factura->id_metodo = $request->id_metodo;
        $factura->emisor = $request->emisor;
        $factura->total = $request->total;
        $factura->observaciones = $request->observaciones;
        $factura->folio_fiscal = $request->folio_fiscal;
        try {
            $factura ->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        return ['information' => $information];
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $factura = Factura::find($id);
        try {
            $factura->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        return ['information' => $information];
    }
}
