<?php

namespace App\Http\Controllers;

use App\Models\Bolsa;
use Illuminate\Http\Request;
use App\Models\Fondo;

class BolsasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bolsas = Bolsa::with('fondo')->get();
        $data = [
            'bolsas' => $bolsas
        ];
        return view('bolsas.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bolsa = new Bolsa();
        $bolsa->numero = '';
        $bolsa->nombre = '';
        $bolsa->id_fondo = '';
        $bolsa->fuente_financiamento = '';
        $fondos = Fondo::all();
        $data = [
            'bolsa' => $bolsa,
            'create' => true,
            'fondos' => $fondos,
        ];
        return view('bolsas.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bolsa = new Bolsa();
        $bolsa->numero = $request->numero;
        $bolsa->nombre = $request->nombre;
        $bolsa->id_fondo = $request->id_fondo;
        $bolsa->fuente_financiamento = $request->fuente_financiamento;
        try {
            $bolsa->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bolsa  $bolsa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bolsa = Bolsa::with('fondo')->find($id);
        $data = [
            'bolsa' => $bolsa
        ];
        return view('bolsas.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bolsa  $bolsa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bolsa = Bolsa::find($id);
        $fondos = Fondo::all();
        $data = [
            'bolsa' => $bolsa,
            'create' => 'false',
            'fondos' => $fondos
        ];
        return view('bolsas.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bolsa  $bolsa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bolsa $bolsa)
    {
        $bolsa = Bolsa::find($request->id);
        $bolsa->numero = $request->numero;
        $bolsa->nombre = $request->nombre;
        $bolsa->id_fondo = $request->id_fondo;
        $bolsa->fuente_financiamento = $request->fuente_financiamento;
        try {
            $bolsa->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bolsa  $bolsa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bolsa = Bolsa::find($id);
        try {
            $bolsa->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }
}
