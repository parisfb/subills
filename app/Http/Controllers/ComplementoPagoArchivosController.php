<?php

namespace App\Http\Controllers;

use App\Models\ComplementoPagoArchivo;
use App\Models\Solicitud;
use Illuminate\Http\Request;

class ComplementoPagoArchivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $complemento_pago_archivos = ComplementoPagoArchivo::with(['solicitud'])->get();
        $data = [
            'complemento_pago_archivos' => $complemento_pago_archivos
        ];
        return view('complemento_pago_archivos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $solicitudes = Solicitud::all();
        $complemento_pago_archivo = new ComplementoPagoArchivo();
        $complemento_pago_archivo->id_solicitud = 0;
        $complemento_pago_archivo->nombre = '';


        $data = [
            'complemento_pago_archivo' => $complemento_pago_archivo,
            'solicitudes' => $solicitudes,
            'create' => true,
        ];

        return view('complemento_pago_archivos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $complemento_pago_archivo = new ComplementoPagoArchivo();
        $complemento_pago_archivo->id_solicitud = $request-> id_solicitud;
        $complemento_pago_archivo->nombre = $request-> nombre;

        try {
            $complemento_pago_archivo->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
            dd($e);
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $complemento_pago_archivo = ComplementoPagoArchivo::with('solicitud')->find($id);
        $data = [
            'complemento_pago_archivo' => $complemento_pago_archivo
        ];
        return view('complemento_pago_archivos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($id)
    {
        $complemento_pago_archivo = ComplementoPagoArchivo::find($id);
        $solicitudes = Solicitud::all();

        $data = [
            'complemento_pago_archivo' => $complemento_pago_archivo,
            'solicitudes' => $solicitudes,
            'create' => 'false',
        ];

        return view('complemento_pago_archivos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $complemento_pago_archivo = ComplementoPagoArchivo::find($request->id);
        $complemento_pago_archivo->id_solicitud = $request-> id_solicitud;
        $complemento_pago_archivo->nombre = $request-> nombre;

        try {
            $complemento_pago_archivo->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
            dd($e);
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $complemento_pago_archivo = ComplementoPagoArchivo::find($id);
        try {
            $complemento_pago_archivo->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}
