<?php

namespace App\Http\Controllers;

use App\Models\Fondo;
use App\Models\Proyecto;
use App\Models\SaldoProyecto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SaldoProyectosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $saldo_proyectos = SaldoProyecto::with(['proyecto', 'fondo'])->get();
        $data = [
            'saldo_proyectos' => $saldo_proyectos
        ];
        return view('saldo_proyectos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $proyectos = Proyecto::all();
        $fondos = Fondo::all();
        $saldo_proyecto = new SaldoProyecto();
        $saldo_proyecto->id_fondo = 0;
        $saldo_proyecto->id_proyecto = 0;
        $saldo_proyecto->adeudo = 0.0;
        $saldo_proyecto->capturado = 0.0;
        $saldo_proyecto->compensado = 0.0;
        $saldo_proyecto->comprobado = 0.0;
        $saldo_proyecto->inicial = 0.0;
        $saldo_proyecto->pagado = 0.0;
        $saldo_proyecto->porcentaje_compensado = 0.0;
        $saldo_proyecto->saldo = 0.0;
        $saldo_proyecto->solicitado = 0.0;

        $data = [
            'saldo_proyecto' => $saldo_proyecto,
            'proyectos' => $proyectos,
            'fondos' => $fondos,
            'create' => true,
        ];

        return view('saldo_proyectos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $saldo_proyecto = new SaldoProyecto();
        $saldo_proyecto->id_fondo = $request-> id_fondo;
        $saldo_proyecto->id_proyecto = $request-> id_proyecto;
        $saldo_proyecto->adeudo = $request-> adeudo;
        $saldo_proyecto->capturado = $request-> capturado;
        $saldo_proyecto->compensado = $request-> compensado;
        $saldo_proyecto->comprobado = $request-> comprobado;
        $saldo_proyecto->inicial = $request-> inicial;
        $saldo_proyecto->pagado = $request-> pagado;
        $saldo_proyecto->porcentaje_compensado = $request-> porcentaje_compensado;
        $saldo_proyecto->saldo = $request-> saldo;
        $saldo_proyecto->solicitado = $request-> solicitado;
        try {
            $saldo_proyecto->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $saldo_proyecto = SaldoProyecto::with(['proyecto', 'fondo'])->find($id);
        $data = [
            'saldo_proyecto' => $saldo_proyecto
        ];
        return view('saldo_proyectos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($id)
    {
        $proyectos = Proyecto::all();
        $fondos = Fondo::all();
        $saldo_proyecto = SaldoProyecto::find($id);

        $data = [
            'saldo_proyecto' => $saldo_proyecto,
            'proyectos' => $proyectos,
            'fondos' => $fondos,
            'create' => 'false',
        ];

        return view('saldo_proyectos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $saldo_proyecto = SaldoProyecto::find($request->id);
        $saldo_proyecto->id_fondo = $request-> id_fondo;
        $saldo_proyecto->id_proyecto = $request-> id_proyecto;
        $saldo_proyecto->adeudo = $request-> adeudo;
        $saldo_proyecto->capturado = $request-> capturado;
        $saldo_proyecto->compensado = $request-> compensado;
        $saldo_proyecto->comprobado = $request-> comprobado;
        $saldo_proyecto->inicial = $request-> inicial;
        $saldo_proyecto->pagado = $request-> pagado;
        $saldo_proyecto->porcentaje_compensado = $request-> porcentaje_compensado;
        $saldo_proyecto->saldo = $request-> saldo;
        $saldo_proyecto->solicitado = $request-> solicitado;
        try {
            $saldo_proyecto->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $saldo_proyecto = SaldoProyecto::find($id);
        try {
            $saldo_proyecto->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}
