<?php

namespace App\Http\Controllers;

use App\Models\Proyecto;
use App\Models\Fondo;
use Illuminate\Http\Request;

class ProyectosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyectos = Proyecto::with('fondo')->get();
        $data = [
            'proyectos' => $proyectos
        ];
        return view('proyectos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyecto = new Proyecto();
        $proyecto->numero = '';
        $proyecto->nombre = '';
        $proyecto->id_fondo = '';
        $fondos = Fondo::all();
        $data = [
            'proyecto' => $proyecto,
            'create' => true,
            'fondos' => $fondos,
        ];
        return view('proyectos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proyecto = new Proyecto();
        $proyecto->numero = $request->numero;
        $proyecto->nombre = $request->nombre;
        $proyecto->id_fondo = $request->id_fondo;
        try {
            $proyecto->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proyecto = Proyecto::with('fondo')->find($id);
        $data = [
            'proyecto' => $proyecto
        ];
        return view('proyectos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = Proyecto::find($id);
        $fondos = Fondo::all();
        $data = [
            'proyecto' => $proyecto,
            'create' => 'false',
            'fondos' => $fondos
        ];
        return view('proyectos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyecto $proyecto)
    {
        $proyecto = Proyecto::find($request->id);
        $proyecto->numero = $request->numero;
        $proyecto->nombre = $request->nombre;
        $proyecto->id_fondo = $request->id_fondo;
        try {
            $proyecto->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        try {
            $proyecto->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }
}
