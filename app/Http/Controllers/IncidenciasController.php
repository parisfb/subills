<?php

namespace App\Http\Controllers;

use App\Models\Incidencia;
use Illuminate\Http\Request;

class IncidenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incidencias = Incidencia::all();
        $data = [
            'incidencias' => $incidencias
          ];
          return view('incidencias.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $incidencia = new Incidencia();  
        $incidencia ->nombre = '';
        $data = [
            'incidencia' => $incidencia,
            'create' => true,
        ];
        return view('incidencias.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $incidencia = new Incidencia();
        $incidencia->nombre = $request->nombre;

        try {
            $incidencia->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $incidencia = Incidencia::find($id);
        $data = [
            'incidencia' => $incidencia
        ];
        return view('incidencias.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $incidencia = Incidencia::find($id);
        $data = [
            'incidencia' => $incidencia,
            'create' => 'false',
        ];
        return view('incidencias.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $incidencia = Incidencia::find($request->id);
        $incidencia->nombre = $request->nombre;

        try {
            $incidencia->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $incidencia = Incidencia::find($id);
        try {
            $incidencia->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data; 
    }
}
