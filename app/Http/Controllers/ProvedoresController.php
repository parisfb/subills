<?php

namespace App\Http\Controllers;

use App\Models\Proveedor;
use Illuminate\Http\Request;
use App\Models\Banco;

class ProvedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provedores = Proveedor::with('banco')->get();
        $data = [
            'provedores' => $provedores
        ];
        return view('provedores.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provedor = new Proveedor();
        $provedor->razon_social = '';
        $provedor->rfc = '';
        $provedor->cuenta = '';
        $provedor->direccion = '';
        $provedor->correo = '';
        $provedor->telefono = '';
        $provedor->observaciones = '';
        $provedor->id_banco = '';
        $provedor->clave = '';
        $bancos = Banco::all();
        $data = [
            'provedor' => $provedor,
            'create' => true,
            'bancos' => $bancos,
        ];
        return view('provedores.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provedor = new Proveedor();
        $provedor->razon_social = $request->razon_social;
        $provedor->rfc = $request->rfc;
        $provedor->cuenta = $request->cuenta;
        $provedor->direccion = $request->direccion;
        $provedor->correo = $request->correo;
        $provedor->telefono = $request->telefono;
        $provedor->observaciones = $request->observaciones;
        $provedor->id_banco = $request->id_banco;
        $provedor->clabe = $request->clabe;
        try {
            $provedor->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proveedor  $provedore
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provedor = Proveedor::with('banco')->find($id);
        $data = [
            'provedor' => $provedor
        ];
        return view('provedores.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proveedor  $provedore
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provedor = Proveedor::find($id);
        $bancos = Banco::all();
        $data = [
            'provedor' => $provedor,
            'create' => 'false',
            'bancos' => $bancos
        ];
        return view('provedores.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proveedor  $provedore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proveedor $provedore)
    {
        $provedor = Proveedor::find($request->id);
        $provedor->razon_social = $request->razon_social;
        $provedor->rfc = $request->rfc;
        $provedor->cuenta = $request->cuenta;
        $provedor->direccion = $request->direccion;
        $provedor->correo = $request->correo;
        $provedor->telefono = $request->telefono;
        $provedor->observaciones = $request->observaciones;
        $provedor->id_banco = $request->id_banco;
        $provedor->clabe = $request->clabe;
        try {
            $provedor->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provedor = Proveedor::find($id);
        try {
            $provedor->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }
}
