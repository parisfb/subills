<?php

namespace App\Http\Controllers;

use App\Models\FormaPago;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class FomaPagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formapagos = FormaPago::all();
        $data = [
            'formapagos' => $formapagos
          ];
          return view('formapagos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $formapago = new FormaPago();
        $formapago ->nombre = '';
        $data = [
            'formapago' => $formapago,
            'create' => true,
        ];
        return view('formapagos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formapago = new FormaPago();
        $formapago->nombre = $request->nombre;

        try {
            $formapago->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FormaPago  $fomaPago
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formapago = FormaPago::find($id);
        $data = [
            'formapago' => $formapago
        ];
        return view('formapagos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FormaPago  $fomaPago
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $formapago = FormaPago::find($id);
        $data = [
            'formapago' => $formapago,
            'create' => 'false',
        ];
        return view('formapagos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FormaPago  $fomaPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $formapago = FormaPago::find($request->id);
        $formapago->nombre = $request->nombre;

        try {
            $formapago->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FormaPago  $fomaPago
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $formapago = FormaPago::find($id);
        try {
            $formapago->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}
