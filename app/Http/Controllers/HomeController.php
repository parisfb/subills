<?php

namespace App\Http\Controllers;

use App\Models\Solicitud;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if($request->order == ''){
            $order = 'id';
        }else{
            $order = $request->order;
        }
        $solicitudes = Solicitud::with(['status', 'proyecto', 'beneficiario', 'fondo', 'incidencia', 'user']) //Devuelve con las objetos que estan relacionados
        ->orderBy($order, 'ASC') //Los ordenda de manera ascendente por id
        ->paginate(3); //Por paginas de x elementos
        $pagination = [
            'total' => $solicitudes->total(),
            'current_page' => $solicitudes->currentPage(),
            'per_page' => $solicitudes->perPage(),
            'last_page' => $solicitudes->lastPage(),
            'from' => $solicitudes->firstItem(),
            'to' => $solicitudes->lastPage(),
        ];
        if($request->ajax()){
            $data = [
                'solicitudes' => $solicitudes,
                'pagination' => $pagination,
            ];
            return $data;
        }else{
            $data = [
                'solicitudes' => json_encode($solicitudes),
                'pagination' => json_encode($pagination),
            ];
            return view('solicitudes.table', $data);
        }
    }
}
