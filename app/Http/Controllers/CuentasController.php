<?php

namespace App\Http\Controllers;

use App\Models\Banco;
use App\Models\Cuenta;
use Illuminate\Http\Request;

class CuentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $cuentas = Cuenta::with('banco')->get();
        $data = [
            'cuentas' => $cuentas
        ];
        return view('cuentas.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $cuenta = new Cuenta();
        $cuenta ->num_cuenta = '';
        $cuenta ->clabe = '';
        $cuenta ->id_banco = '';
        $bancos = Banco::all();
        $data = [
            'cuenta' => $cuenta,
            'create' => true,
            'bancos' => $bancos,
        ];
        return view('cuentas.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $cuenta = new Cuenta();
        $cuenta->num_cuenta = $request ->num_cuenta;
        $cuenta->id_banco = $request -> id_banco;
        $cuenta->clabe = $request -> clabe;
        try {
            $cuenta->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.*/
    public function show($id)
    {
        $cuenta = Cuenta::with('banco')->find($id);
        $data = [
            'cuenta' => $cuenta
        ];
        return view('cuentas.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cuenta  $cuenta
     */
    public function edit($id)
    {
        $cuenta = Cuenta::find($id);
        $bancos = Banco::all();
        $data = [
            'cuenta' => $cuenta,
            'create' => 'false',
            'bancos' => $bancos
        ];
        return view('cuentas.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $cuenta = Cuenta::find($request->id);
        $cuenta -> num_cuenta = $request->num_cuenta;
        $cuenta -> clabe = $request->clabe;
        $cuenta -> id_banco = $request->id_banco;
        try {
            $cuenta->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $cuenta = Cuenta::find($id);
        try {
            $cuenta->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}
