<?php

namespace App\Http\Controllers;

use App\Models\Compensaciones;
use App\Models\Status;
use Illuminate\Http\Request;

class CompensacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compensaciones = Compensaciones::with('status')->get();
        $data = [
            'compensaciones' => $compensaciones
        ];
        return view('compensaciones.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $compensacion = new Compensaciones();
        $compensacion->id_status = '';
        $compensacion->fecha = '';
        $compensacion->folio = '';
        $compensacion->destino = '';
        $compensacion->justificacion = '';
        $compensacion->monto = '';
        $status = Status::all();
        $data = [
            'compensacion' => $compensacion,
            'create' => true,
            'status' => $status,
        ];
        return view('compensaciones.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $compensacion = new Compensaciones();
        $compensacion->id_status = $request->id_status;
        $compensacion->fecha = $request->fecha;
        $compensacion->folio = $request->folio;
        $compensacion->destino = $request->destino;
        $compensacion->justificacion = $request->justificacion;
        $compensacion->monto = $request->monto;
        try {
            $compensacion->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        return ['information' => $information];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Compensaciones  $compensacione
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $compensacion = Compensaciones::with('status')->find($id);
        $data = [
            'compensacion' => $compensacion
        ];
        return view('compensaciones.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Compensaciones  $compensacione
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $compensacion = Compensaciones::find($id);
        $status = Status::all();
        $data = [
            'compensacion' => $compensacion,
            'create' => 'false',
            'status' => $status
        ];
        return view('compensaciones.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $compensacion = Compensaciones::find($request->id);
        $compensacion->id_status = $request->id_status;
        $compensacion->fecha = $request->fecha;
        $compensacion->folio = $request->folio;
        $compensacion->destino = $request->destino;
        $compensacion->justificacion = $request->justificacion;
        $compensacion->monto = $request->monto;
        try {
            $compensacion->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        return ['information' => $information];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Compensaciones  $compensacione
     */
    public function destroy($id)
    {
        $compensacion = Compensaciones::find($id);
        try {
            $compensacion->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        return ['information' => $information];
    }
}
