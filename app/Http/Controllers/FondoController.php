<?php

namespace App\Http\Controllers;

use App\Models\Fondo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class FondoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fondos = Fondo::all();
        $data = [
          'fondos' => $fondos
        ];
        return view('fondos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $fondo = new Fondo();
        $fondo ->numero = '';
        $fondo ->nombre = '';
        $data = [
            'fondo' => $fondo,
            'create' => true,
        ];
        return view('fondos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     */
    public function store(Request $request)
    {
        $fondo = new Fondo();
        $fondo->nombre = $request -> nombre;
        $fondo->numero = $request -> numero;
        try {
            $fondo->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;

    }

    /**
     * Display the specified resource.
     *

     */
    public function show($id)
    {

        $fondo = Fondo::find($id);
        $data = [
            'fondo' => $fondo
        ];
        return view('fondos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fondo  $fondo
     */
    public function edit($id)
    {
        $fondo = Fondo::find($id);
        $data = [
            'fondo' => $fondo,
            'create' => 'false',
        ];
        return view('fondos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     */
    public function update(Request $request)
    {
        $fondo = Fondo::find($request->id);
        $fondo -> nombre = $request->nombre;
        $fondo -> numero = $request->numero;
        try {
            $fondo->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $fondo = Fondo::find($id);
        try {
            $fondo->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}
