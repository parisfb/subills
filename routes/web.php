<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/fondos/create', 'FondoController@create')->name('fondosCreate');
Route::get('/fondos/edit/{id}', 'FondoController@edit')->name('fondosEdit');
Route::apiResource('fondos', 'FondoController');

Route::get('/solicitudes/create', 'SolicitudesController@create')->name('solicitudesCreate');
Route::get('/solicitudes/edit/{id}', 'SolicitudesController@edit')->name('solicitudesEdit');
Route::get('/solicitudes/buscar', 'SolicitudesController@buscar')->name('solicitudesBuscar');
Route::apiResource('solicitudes', 'SolicitudesController');

Route::get('/saldo_proyectos/create', 'SaldoProyectosController@create')->name('saldo_proyectosCreate');
Route::get('/saldo_proyectos/edit/{id}', 'SaldoProyectosController@edit')->name('saldo_proyectosEdit');
Route::apiResource('saldo_proyectos', 'SaldoProyectosController');

Route::get('/saldo_partidas/create', 'SaldoPartidasController@create')->name('saldo_partidasCreate');
Route::get('/saldo_partidas/edit/{id}', 'SaldoPartidasController@edit')->name('saldo_partidasEdit');
Route::apiResource('saldo_partidas', 'SaldoPartidasController');

Route::get('/afintipos/create', 'AfinTiposController@create')->name('afintiposCreate');
Route::get('/afintipos/edit/{id}', 'AfinTiposController@edit')->name('afintiposEdit');
Route::apiResource('afintipos', 'AfinTiposController');

Route::get('/bancos/create', 'BancosController@create')->name('bancosCreate');
Route::get('/bancos/edit/{id}', 'BancosController@edit')->name('bancosEdit');
Route::apiResource('bancos', 'BancosController');

Route::get('/cuentas/create', 'CuentasController@create')->name('cuentasCreate');
Route::get('/cuentas/edit/{id}', 'CuentasController@edit')->name('cuentasEdit');
Route::apiResource('cuentas', 'CuentasController');

Route::get('/dependencias/create', 'DependenciasController@create')->name('dependenciasCreate');
Route::get('/dependencias/edit/{id}', 'DependenciasController@edit')->name('dependenciasEdit');
Route::apiResource('dependencias', 'DependenciasController');

Route::get('/formapagos/create', 'FomaPagosController@create')->name('formapagosCreate');
Route::get('/formapagos/edit/{id}', 'FomaPagosController@edit')->name('formapagosEdit');
Route::apiResource('formapagos', 'FomaPagosController');

Route::get('/incidencias/create', 'IncidenciasController@create')->name('incidenciasCreate');
Route::get('/incidencias/edit/{id}', 'IncidenciasController@edit')->name('incidenciasEdit');
Route::apiResource('incidencias', 'IncidenciasController');

Route::get('/metodos/create', 'MetodosController@create')->name('metodosCreate');
Route::get('/metodos/edit/{id}', 'MetodosController@edit')->name('metodosEdit');
Route::apiResource('metodos', 'MetodosController');

Route::get('/status/create', 'StatusController@create')->name('statusCreate');
Route::get('/status/edit/{id}', 'StatusController@edit')->name('statusEdit');
Route::apiResource('status', 'StatusController');

Route::get('/paquetes/create', 'PaquetesController@create')->name('paquetesCreate');
Route::get('/paquetes/edit/{id}', 'PaquetesController@edit')->name('paquetesEdit');
Route::apiResource('paquetes', 'PaquetesController');

Route::get('/paquetes_solicitudes/create', 'PaquetesSolicitudesController@create')->name('paquetesSolicitudesCreate');
Route::get('/paquetes_solicitudes/edit/{id}', 'PaquetesSolicitudesController@edit')->name('paquetesSolicitudesEdit');
Route::apiResource('paquetes_solicitudes', 'PaquetesSolicitudesController');

Route::get('/anexos/create', 'AnexosController@create')->name('anexosCreate');
Route::get('/anexos/edit/{id}', 'AnexosController@edit')->name('anexosEdit');
Route::apiResource('anexos', 'AnexosController');

Route::get('/beneficiarios/create', 'BeneficiariosController@create')->name('beneficiariosCreate');
Route::get('/beneficiarios/edit/{id}', 'BeneficiariosController@edit')->name('beneficiariosEdit');
Route::apiResource('beneficiarios', 'BeneficiariosController');

Route::get('/bolsas/create', 'BolsasController@create')->name('bolsasCreate');
Route::get('/bolsas/edit/{id}', 'BolsasController@edit')->name('bolsasEdit');
Route::apiResource('bolsas', 'BolsasController');

Route::get('/compensaciones/create', 'CompensacionesController@create')->name('compensacionesCreate');
Route::get('/compensaciones/edit/{id}', 'CompensacionesController@edit')->name('compensacionesEdit');
Route::apiResource('compensaciones', 'CompensacionesController');

Route::get('/facturas/create', 'FacturasController@create')->name('facturasCreate');
Route::get('/facturas/edit/{id}', 'FacturasController@edit')->name('facturasEdit');
Route::apiResource('facturas', 'FacturasController');

Route::get('/compensaciones_origen/create', 'CompensacionesOrigenController@create')->name('compensaciones_origenCreate');
Route::get('/compensaciones_origen/edit/{id}', 'CompensacionesOrigenController@edit')->name('compensaciones_origenEdit');
Route::apiResource('compensaciones_origen', 'CompensacionesOrigenController');

Route::get('/provedores/create', 'ProvedoresController@create')->name('provedoresCreate');
Route::get('/provedores/edit/{id}', 'ProvedoresController@edit')->name('provedoresEdit');
Route::apiResource('provedores', 'ProvedoresController');

Route::get('/partidas/create', 'PartidasController@create')->name('partidasCreate');
Route::get('/partidas/edit/{id}', 'PartidasController@edit')->name('partidasEdit');
Route::apiResource('partidas', 'PartidasController');

Route::get('/proyectos/create', 'ProyectosController@create')->name('proyectosCreate');
Route::get('/proyectos/edit/{id}', 'ProyectosController@edit')->name('proyectosEdit');
Route::apiResource('proyectos', 'ProyectosController');

Route::get('/movimientos/create', 'MovimientosController@create')->name('movimientosCreate');
Route::get('/movimientos/edit/{id}', 'MovimientosController@edit')->name('movimientosEdit');
Route::apiResource('movimientos', 'MovimientosController');

Route::get('/reintegros/create', 'ReintegrosController@create')->name('reintegrosCreate');
Route::get('/reintegros/edit/{id}', 'ReintegrosController@edit')->name('reintegrosEdit');
Route::apiResource('reintegros', 'ReintegrosController');

Route::get('/complemento_pagos/create', 'ComplementoPagosController@create')->name('complementoPagoCreate');
Route::get('/complemento_pagos/edit/{id}', 'ComplementoPagosController@edit')->name('complementoPagoEdit');
Route::apiResource('complemento_pagos', 'ComplementoPagosController');

Route::get('/complemento_pago_archivos/create', 'ComplementoPagoArchivosController@create')->name('complementoPagoArchivosCreate');
Route::get('/complemento_pago_archivos/edit/{id}', 'ComplementoPagoArchivosController@edit')->name('complementoPagoArchivosEdit');
Route::apiResource('complemento_pago_archivos', 'ComplementoPagoArchivosController');

Route::get('/factura_archivos/create', 'FacturaArchivosController@create')->name('facturaArchivosCreate');
Route::get('/factura_archivos/edit/{id}', 'FacturaArchivosController@edit')->name('facturArchivosEdit');
Route::apiResource('factura_archivos', 'FacturaArchivosController');
