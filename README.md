# SUBILLS SERVICIOS UNIVERSITARIOS BILLS

_Aplicacion Web para la gestion de licitaciones, egresos e ingresos del departamento de Finanzas de la Coordinación General de Servicios Universitarios_

**Instrucciones para instalar:**

1. Poner las credenciales necesarias en .env 

2. Ecribir los siguientes comandos:
    - php artisan cache:clear
    - php artisan key:generate
    - composer install
    - npm install
    - php artisan migrate:refresh --seed
    - php artisan voyager:admin your@email.com --create
    - php artisan serve
