@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <create-edit-status-component v-bind:create="{{$create}}" :status="{{$status}}" />
        </div>
    </div>
@endsection
  