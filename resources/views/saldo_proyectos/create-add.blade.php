@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <create-edit-saldoproyectos-component :fondos="{{$fondos}}"
                                             :proyectos="{{$proyectos}}"
                                             :create="{{$create}}"
                                             :saldo_proyecto="{{$saldo_proyecto}}" >
            </create-edit-saldoproyectos-component>
        </div>
    </div>
@endsection
