/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : 127.0.0.1:3306
 Source Schema         : subills

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 17/06/2021 23:43:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for afin_tipos
-- ----------------------------
DROP TABLE IF EXISTS `afin_tipos`;
CREATE TABLE `afin_tipos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of afin_tipos
-- ----------------------------
BEGIN;
INSERT INTO `afin_tipos` VALUES (1, 'ADEUDO', '2021-05-19 04:42:29', '2021-06-08 18:47:01');
INSERT INTO `afin_tipos` VALUES (2, 'COMPRA', '2021-06-08 18:47:26', '2021-06-08 18:47:26');
INSERT INTO `afin_tipos` VALUES (3, 'VENTA', '2021-06-08 18:47:29', '2021-06-08 18:47:29');
INSERT INTO `afin_tipos` VALUES (4, 'PRESTAMO', '2021-06-08 18:47:34', '2021-06-08 18:47:34');
INSERT INTO `afin_tipos` VALUES (5, 'GASTO OPERATIVO', '2021-06-08 18:48:07', '2021-06-08 18:48:07');
INSERT INTO `afin_tipos` VALUES (6, 'Nuevo desde Voy', '2021-06-15 18:45:05', '2021-06-15 18:45:05');
COMMIT;

-- ----------------------------
-- Table structure for anexos
-- ----------------------------
DROP TABLE IF EXISTS `anexos`;
CREATE TABLE `anexos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_solicitud` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_solicitud_anexo_fk` (`id_solicitud`),
  CONSTRAINT `id_solicitud_anexo_fk` FOREIGN KEY (`id_solicitud`) REFERENCES `solicitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of anexos
-- ----------------------------
BEGIN;
INSERT INTO `anexos` VALUES (2, 'OTRO ANEXO', '2021-05-28 17:49:12', '2021-06-08 19:32:55', 4);
INSERT INTO `anexos` VALUES (3, 'ANEXO UNO', '2021-06-08 19:33:09', '2021-06-08 19:33:09', 4);
INSERT INTO `anexos` VALUES (4, 'ANEXO DOS', '2021-06-08 19:33:13', '2021-06-08 19:33:13', 5);
INSERT INTO `anexos` VALUES (5, 'ANEXO TRES', '2021-06-08 19:33:18', '2021-06-08 19:33:18', 6);
INSERT INTO `anexos` VALUES (6, 'ANEXO CUATRO', '2021-06-08 19:33:23', '2021-06-08 19:33:23', 1);
INSERT INTO `anexos` VALUES (7, 'ANEXO CINCO', '2021-06-08 19:33:29', '2021-06-08 19:33:29', 4);
COMMIT;

-- ----------------------------
-- Table structure for bancos
-- ----------------------------
DROP TABLE IF EXISTS `bancos`;
CREATE TABLE `bancos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of bancos
-- ----------------------------
BEGIN;
INSERT INTO `bancos` VALUES (2, 'Santander', '2021-05-12 02:12:05', '2021-05-12 02:12:05');
INSERT INTO `bancos` VALUES (3, 'BBVA', '2021-06-08 18:22:51', '2021-05-12 02:12:17');
INSERT INTO `bancos` VALUES (4, 'Citibanamex', '2021-05-12 02:12:27', '2021-05-12 02:12:27');
INSERT INTO `bancos` VALUES (5, 'Banco Azteca', '2021-06-08 18:23:18', '2021-06-08 18:23:18');
INSERT INTO `bancos` VALUES (6, 'Banco del Bajío', '2021-06-08 18:23:30', '2021-06-08 18:23:30');
INSERT INTO `bancos` VALUES (7, 'Banco Inbursa', '2021-06-08 18:23:36', '2021-06-08 18:23:36');
INSERT INTO `bancos` VALUES (8, 'Banorte', '2021-06-08 18:23:50', '2021-06-08 18:23:50');
INSERT INTO `bancos` VALUES (9, 'CIBanco', '2021-06-08 18:23:59', '2021-06-08 18:23:59');
INSERT INTO `bancos` VALUES (10, 'HSBC', '2021-06-08 18:24:09', '2021-06-08 18:24:09');
INSERT INTO `bancos` VALUES (11, 'Scotiabank', '2021-06-08 18:24:14', '2021-06-08 18:24:14');
COMMIT;

-- ----------------------------
-- Table structure for beneficiarios
-- ----------------------------
DROP TABLE IF EXISTS `beneficiarios`;
CREATE TABLE `beneficiarios` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `cuenta` varchar(25) DEFAULT NULL,
  `id_dependencia` bigint unsigned DEFAULT NULL,
  `codigo` int DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dependencia_beneficiario_fk` (`id_dependencia`),
  CONSTRAINT `id_dependencia_beneficiario_fk` FOREIGN KEY (`id_dependencia`) REFERENCES `dependencias` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of beneficiarios
-- ----------------------------
BEGIN;
INSERT INTO `beneficiarios` VALUES (1, 'Paris Beneficiario', '4587895', 1, 217758, 'Nueva obsercacion', '2021-05-19 04:46:54', '2021-05-19 04:46:54');
INSERT INTO `beneficiarios` VALUES (2, 'Oscar Beneficiario', '2112312412', 8, 207512147, 'Sin obs', '2021-06-08 18:37:25', '2021-06-08 18:37:25');
INSERT INTO `beneficiarios` VALUES (3, 'Juan Carlos Beneficiario', '123123412', 1, 314512341, 'Sin obs', '2021-06-08 18:37:46', '2021-06-08 18:37:46');
INSERT INTO `beneficiarios` VALUES (4, 'Sinaí Beneficiario', '206989575', 1, 206989575, 'Sin obs', '2021-06-08 18:38:20', '2021-06-08 18:38:20');
INSERT INTO `beneficiarios` VALUES (5, 'Emerson Beneficiario', '207509057', 6, 207509057, 'Sin obs', '2021-06-08 18:38:48', '2021-06-08 18:38:48');
COMMIT;

-- ----------------------------
-- Table structure for bolsas
-- ----------------------------
DROP TABLE IF EXISTS `bolsas`;
CREATE TABLE `bolsas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `numero` int DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `id_fondo` bigint unsigned DEFAULT NULL,
  `fuente_financiamento` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_fondo_bolsa_fk` (`id_fondo`),
  CONSTRAINT `id_fondo_bolsa_fk` FOREIGN KEY (`id_fondo`) REFERENCES `fondos` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of bolsas
-- ----------------------------
BEGIN;
INSERT INTO `bolsas` VALUES (4, 1932, 'SUBSIDIO ORDINARIO GOBIERNO ESTATAL 2021 SERUNI', 5, 'SUBSIDIO ORDINARIO GOBIERNO ESTATAL', '2021-06-08 18:14:28', '2021-06-08 18:14:28');
INSERT INTO `bolsas` VALUES (5, 12201, 'APOYOS FEDERALES', 15, 'CONACULTA VARIOS', '2021-06-08 18:17:40', '2021-06-08 18:17:40');
INSERT INTO `bolsas` VALUES (6, 12202, 'CONACULTA', 16, 'CONACULTA PAPIROLAS', '2021-06-08 18:18:07', '2021-06-08 18:18:07');
INSERT INTO `bolsas` VALUES (7, 12231, 'APOYOS ESTATALES', 13, 'COECYTJAL', '2021-06-08 18:18:40', '2021-06-08 18:18:40');
INSERT INTO `bolsas` VALUES (8, 12312, 'APOYOS MUNICIPALES', 9, 'APOYO MUNICIPAL', '2021-06-08 18:19:12', '2021-06-08 18:19:12');
COMMIT;

-- ----------------------------
-- Table structure for compensaciones
-- ----------------------------
DROP TABLE IF EXISTS `compensaciones`;
CREATE TABLE `compensaciones` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_status` bigint unsigned DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `folio` mediumint DEFAULT NULL,
  `destino` bigint unsigned DEFAULT NULL,
  `justificacion` varchar(255) DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_status_compensciones_fk` (`id_status`),
  CONSTRAINT `id_status_compensciones_fk` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of compensaciones
-- ----------------------------
BEGIN;
INSERT INTO `compensaciones` VALUES (1, 1, '2021-05-22 00:00:00', 12, 212, 'Sobro', 2145.45, '2021-05-19 04:50:33', '2021-05-19 04:50:33');
INSERT INTO `compensaciones` VALUES (4, 2, '2021-06-08 00:00:00', 123, 123, 'N/A', 123.00, '2021-06-08 19:06:33', '2021-06-08 19:06:33');
INSERT INTO `compensaciones` VALUES (5, 3, '2021-06-08 00:00:00', 124, 124, 'N/A', 124.00, '2021-06-08 19:06:43', '2021-06-08 19:06:43');
INSERT INTO `compensaciones` VALUES (6, 4, '2021-06-30 00:00:00', 333, 333, 'N/A', 33124.00, '2021-06-08 19:06:58', '2021-06-08 19:06:58');
INSERT INTO `compensaciones` VALUES (7, 5, '2021-06-13 00:00:00', 1313, 1313, 'N/A', 1313.00, '2021-06-08 19:07:08', '2021-06-08 19:07:08');
COMMIT;

-- ----------------------------
-- Table structure for compensaciones_origen
-- ----------------------------
DROP TABLE IF EXISTS `compensaciones_origen`;
CREATE TABLE `compensaciones_origen` (
  `id_compensacion` bigint unsigned DEFAULT NULL,
  `id_partida` bigint unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `id_partida_comorig_fk` (`id_partida`),
  KEY `id_comp_comporig_fk` (`id_compensacion`),
  CONSTRAINT `id_comp_comporig_fk` FOREIGN KEY (`id_compensacion`) REFERENCES `compensaciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_partida_comorig_fk` FOREIGN KEY (`id_partida`) REFERENCES `partidas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of compensaciones_origen
-- ----------------------------
BEGIN;
INSERT INTO `compensaciones_origen` VALUES (1, 8, '2021-06-08 22:57:57', '2021-05-28 17:36:11', 3);
INSERT INTO `compensaciones_origen` VALUES (1, 7, '2021-06-08 19:08:04', '2021-06-08 19:08:04', 4);
INSERT INTO `compensaciones_origen` VALUES (4, 8, '2021-06-08 19:08:10', '2021-06-08 19:08:10', 5);
INSERT INTO `compensaciones_origen` VALUES (5, 9, '2021-06-08 19:08:17', '2021-06-08 19:08:17', 6);
INSERT INTO `compensaciones_origen` VALUES (7, 10, '2021-06-08 19:08:25', '2021-06-08 19:08:25', 7);
COMMIT;

-- ----------------------------
-- Table structure for complemento_pago_archivos
-- ----------------------------
DROP TABLE IF EXISTS `complemento_pago_archivos`;
CREATE TABLE `complemento_pago_archivos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_solicitud` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_solicitud_compago_arch_fk` (`id_solicitud`),
  CONSTRAINT `id_solicitud_compago_arch_fk` FOREIGN KEY (`id_solicitud`) REFERENCES `solicitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of complemento_pago_archivos
-- ----------------------------
BEGIN;
INSERT INTO `complemento_pago_archivos` VALUES (2, 'Archivo 5', '2021-05-28 21:57:38', '2021-06-08 19:35:58', 1);
INSERT INTO `complemento_pago_archivos` VALUES (3, 'Archivo 1', '2021-06-08 19:36:23', '2021-06-08 19:36:23', 1);
INSERT INTO `complemento_pago_archivos` VALUES (4, 'Archivo 2', '2021-06-08 19:36:27', '2021-06-08 19:36:27', 4);
INSERT INTO `complemento_pago_archivos` VALUES (5, 'Archivo 4', '2021-06-08 19:36:32', '2021-06-08 19:36:32', 5);
INSERT INTO `complemento_pago_archivos` VALUES (6, 'Archivo 3', '2021-06-08 19:36:37', '2021-06-08 19:36:37', 6);
COMMIT;

-- ----------------------------
-- Table structure for complemento_pagos
-- ----------------------------
DROP TABLE IF EXISTS `complemento_pagos`;
CREATE TABLE `complemento_pagos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_solicitud` bigint unsigned DEFAULT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `factura_ingreso` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_solicitud_compago_fk` (`id_solicitud`),
  CONSTRAINT `id_solicitud_compago_fk` FOREIGN KEY (`id_solicitud`) REFERENCES `solicitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of complemento_pagos
-- ----------------------------
BEGIN;
INSERT INTO `complemento_pagos` VALUES (3, 1, '2021-06-08 00:00:00', 'N/A', '1111', '2021-06-08 19:34:14', '2021-06-08 19:34:14');
INSERT INTO `complemento_pagos` VALUES (4, 4, '2021-06-08 00:00:00', 'N/A', '121212', '2021-06-08 19:34:21', '2021-06-08 19:34:21');
INSERT INTO `complemento_pagos` VALUES (5, 5, '2021-06-08 00:00:00', 'N/A', '131313', '2021-06-08 19:34:27', '2021-06-08 19:34:27');
INSERT INTO `complemento_pagos` VALUES (6, 6, '2021-06-13 00:00:00', 'N/A', '151515', '2021-06-08 19:34:37', '2021-06-08 19:34:37');
INSERT INTO `complemento_pagos` VALUES (7, 1, '2021-06-15 00:00:00', 'Sin observaciones', '151515', '2021-06-08 19:35:04', '2021-06-08 19:35:04');
COMMIT;

-- ----------------------------
-- Table structure for cuentas
-- ----------------------------
DROP TABLE IF EXISTS `cuentas`;
CREATE TABLE `cuentas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `num_cuenta` varchar(25) DEFAULT NULL,
  `id_banco` bigint unsigned DEFAULT NULL,
  `clabe` varchar(18) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_banco_cuenta_fk` (`id_banco`),
  CONSTRAINT `id_banco_cuenta_fk` FOREIGN KEY (`id_banco`) REFERENCES `bancos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of cuentas
-- ----------------------------
BEGIN;
INSERT INTO `cuentas` VALUES (2, '454874545', 2, '5451212453212', '2021-05-12 00:40:52', '2021-05-12 00:40:55');
INSERT INTO `cuentas` VALUES (4, '109249982', 4, '002010077777777779', '2021-06-08 18:26:13', '2021-06-08 18:26:13');
INSERT INTO `cuentas` VALUES (5, '12349249982', 2, '002010077777777771', '2021-06-08 18:26:24', '2021-06-08 18:26:24');
INSERT INTO `cuentas` VALUES (6, '534234249982', 6, '032180000118359719', '2021-06-08 18:26:36', '2021-06-08 18:26:36');
INSERT INTO `cuentas` VALUES (7, '512343234123', 8, '014027000005555558', '2021-06-08 18:26:50', '2021-06-08 18:26:50');
INSERT INTO `cuentas` VALUES (8, '432344123431', 10, '002555000000000003', '2021-06-08 18:27:02', '2021-06-08 18:27:02');
INSERT INTO `cuentas` VALUES (9, '77777771', 11, '002555012312300003', '2021-06-08 18:27:28', '2021-06-08 18:27:28');
COMMIT;

-- ----------------------------
-- Table structure for data_rows
-- ----------------------------
DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int unsigned NOT NULL,
  `field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `order` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of data_rows
-- ----------------------------
BEGIN;
INSERT INTO `data_rows` VALUES (1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4);
INSERT INTO `data_rows` VALUES (4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 5);
INSERT INTO `data_rows` VALUES (5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 6);
INSERT INTO `data_rows` VALUES (6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 7);
INSERT INTO `data_rows` VALUES (7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9);
INSERT INTO `data_rows` VALUES (8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 10);
INSERT INTO `data_rows` VALUES (9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12);
INSERT INTO `data_rows` VALUES (10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 13);
INSERT INTO `data_rows` VALUES (11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 14);
INSERT INTO `data_rows` VALUES (12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1);
INSERT INTO `data_rows` VALUES (13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2);
INSERT INTO `data_rows` VALUES (14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3);
INSERT INTO `data_rows` VALUES (15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4);
INSERT INTO `data_rows` VALUES (16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (18, 3, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 4);
INSERT INTO `data_rows` VALUES (19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 5);
INSERT INTO `data_rows` VALUES (20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '{}', 3);
INSERT INTO `data_rows` VALUES (21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 11);
INSERT INTO `data_rows` VALUES (23, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 8);
INSERT INTO `data_rows` VALUES (41, 1, 'phone', 'text', 'Phone', 1, 1, 1, 1, 1, 1, '{}', 13);
INSERT INTO `data_rows` VALUES (43, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (44, 8, 'nombre', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (45, 8, 'abreviatura', 'text', 'Abreviatura', 1, 1, 1, 1, 1, 1, '{}', 3);
INSERT INTO `data_rows` VALUES (46, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 5);
INSERT INTO `data_rows` VALUES (47, 8, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 6);
INSERT INTO `data_rows` VALUES (48, 1, 'user_belongsto_dependencia_relationship', 'relationship', 'dependencias', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Dependencia\",\"table\":\"dependencias\",\"type\":\"belongsTo\",\"column\":\"id_dependencia\",\"key\":\"id\",\"label\":\"abreviatura\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 15);
INSERT INTO `data_rows` VALUES (49, 1, 'id_dependencia', 'text', 'Id Dependencia', 0, 1, 1, 1, 1, 1, '{}', 14);
INSERT INTO `data_rows` VALUES (51, 9, 'id_user', 'text', 'Id User', 0, 1, 1, 1, 1, 1, '{}', 1);
INSERT INTO `data_rows` VALUES (52, 9, 'id_dependencia', 'text', 'Id Dependencia', 0, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (53, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 5);
INSERT INTO `data_rows` VALUES (54, 9, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 6);
INSERT INTO `data_rows` VALUES (55, 9, 'responsable_dependencia_belongsto_dependencia_relationship', 'relationship', 'Dependencias', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Dependencia\",\"table\":\"dependencias\",\"type\":\"belongsTo\",\"column\":\"id_dependencia\",\"key\":\"id\",\"label\":\"nombre\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3);
INSERT INTO `data_rows` VALUES (56, 9, 'responsable_dependencia_belongsto_user_relationship', 'relationship', 'Users', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4);
INSERT INTO `data_rows` VALUES (57, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (58, 11, 'nombre', 'text', 'Nombre', 0, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (59, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 3);
INSERT INTO `data_rows` VALUES (60, 11, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 4);
INSERT INTO `data_rows` VALUES (61, 12, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (62, 12, 'nombre', 'text', 'Nombre', 0, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (63, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 3);
INSERT INTO `data_rows` VALUES (64, 12, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 4);
INSERT INTO `data_rows` VALUES (65, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (66, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (67, 13, 'nombre', 'text', 'Nombre', 0, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (68, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 3);
INSERT INTO `data_rows` VALUES (69, 13, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 4);
INSERT INTO `data_rows` VALUES (70, 13, 'forma_pago_hasmany_factura_relationship', 'relationship', 'facturas', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Factura\",\"table\":\"facturas\",\"type\":\"hasMany\",\"column\":\"id_forma_pago\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"afin_tipos\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5);
INSERT INTO `data_rows` VALUES (71, 12, 'metodo_hasmany_factura_relationship', 'relationship', 'facturas', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Factura\",\"table\":\"facturas\",\"type\":\"hasMany\",\"column\":\"id_metodo\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"afin_tipos\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5);
INSERT INTO `data_rows` VALUES (72, 14, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (73, 14, 'nombre', 'text', 'Nombre', 0, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (74, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 3);
INSERT INTO `data_rows` VALUES (75, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4);
INSERT INTO `data_rows` VALUES (77, 14, 'banco_hasmany_proveedore_relationship', 'relationship', 'Proveedores', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Proveedor\",\"table\":\"proveedores\",\"type\":\"hasMany\",\"column\":\"id_banco\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"afin_tipos\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5);
INSERT INTO `data_rows` VALUES (78, 14, 'banco_hasmany_cuenta_relationship', 'relationship', 'Cuentas', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Cuenta\",\"table\":\"cuentas\",\"type\":\"hasMany\",\"column\":\"id_banco\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"afin_tipos\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6);
INSERT INTO `data_rows` VALUES (79, 15, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1);
INSERT INTO `data_rows` VALUES (80, 15, 'concepto', 'text', 'Concepto', 0, 1, 1, 1, 1, 1, '{}', 2);
INSERT INTO `data_rows` VALUES (81, 15, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 3);
INSERT INTO `data_rows` VALUES (82, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 4);
INSERT INTO `data_rows` VALUES (83, 15, 'status_hasmany_factura_relationship', 'relationship', 'Facturas', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Factura\",\"table\":\"facturas\",\"type\":\"hasMany\",\"column\":\"id_status\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"afin_tipos\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5);
INSERT INTO `data_rows` VALUES (84, 15, 'status_hasmany_solicitude_relationship', 'relationship', 'Solicitudes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Solicitud\",\"table\":\"solicitudes\",\"type\":\"hasMany\",\"column\":\"id_status\",\"key\":\"id\",\"label\":\"folio\",\"pivot_table\":\"afin_tipos\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6);
INSERT INTO `data_rows` VALUES (85, 15, 'status_hasmany_compensacione_relationship', 'relationship', 'Compensaciones', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Compensaciones\",\"table\":\"compensaciones\",\"type\":\"hasMany\",\"column\":\"id_status\",\"key\":\"id\",\"label\":\"folio\",\"pivot_table\":\"afin_tipos\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7);
COMMIT;

-- ----------------------------
-- Table structure for data_types
-- ----------------------------
DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of data_types
-- ----------------------------
BEGIN;
INSERT INTO `data_types` VALUES (1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-04-13 20:03:30', '2021-05-19 22:58:40');
INSERT INTO `data_types` VALUES (2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `data_types` VALUES (3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-04-13 20:03:30', '2021-06-15 18:57:59');
INSERT INTO `data_types` VALUES (8, 'dependencias', 'dependencias', 'Dependencia', 'Dependencias', 'voyager-company', 'App\\Dependencia', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-14 03:51:04', '2021-06-15 18:56:16');
INSERT INTO `data_types` VALUES (9, 'responsable_dependencias', 'responsable-dependencias', 'Responsable Dependencia', 'Responsable Dependencias', 'voyager-medal-rank-star', 'App\\ResponsableDependencia', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-14 04:25:50', '2021-06-18 04:42:21');
INSERT INTO `data_types` VALUES (11, 'afin_tipos', 'afin-tipos', 'Afin Tipo', 'Afin Tipos', 'voyager-paw', 'App\\Models\\AfinTipo', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"nombre\",\"order_display_column\":\"nombre\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-15 18:44:12', '2021-06-15 18:55:19');
INSERT INTO `data_types` VALUES (12, 'metodos', 'metodos', 'Metodo', 'Metodos', 'voyager-window-list', 'App\\Models\\Metodo', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"nombre\",\"order_display_column\":\"nombre\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-15 18:52:36', '2021-06-18 04:15:59');
INSERT INTO `data_types` VALUES (13, 'forma_pagos', 'forma-pagos', 'Forma Pago', 'Forma Pagos', 'voyager-buy', 'App\\Models\\FormaPago', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-15 19:01:15', '2021-06-18 04:16:36');
INSERT INTO `data_types` VALUES (14, 'bancos', 'bancos', 'Banco', 'Bancos', 'voyager-treasure', 'App\\Models\\Banco', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-15 19:25:35', '2021-06-18 04:40:47');
INSERT INTO `data_types` VALUES (15, 'status', 'status', 'Status', 'Status', 'voyager-info-circled', 'App\\Models\\Status', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-15 19:42:50', '2021-06-18 04:41:27');
COMMIT;

-- ----------------------------
-- Table structure for dependencias
-- ----------------------------
DROP TABLE IF EXISTS `dependencias`;
CREATE TABLE `dependencias` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abreviatura` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dependencias
-- ----------------------------
BEGIN;
INSERT INTO `dependencias` VALUES (1, 'COORDINACION GENERAL DE SERVICIOS A UNIVERSITARIOS', 'CGSU', '2021-04-14 03:54:00', '2021-04-14 04:23:01');
INSERT INTO `dependencias` VALUES (2, 'COMPLEJO DEPORTIVO UNIVERSITARIO', 'CDU', '2021-04-14 04:14:00', '2021-04-14 04:22:16');
INSERT INTO `dependencias` VALUES (5, 'COORDINACION DE TECNOLOGIAS PARA EL APRENDIZAJE', 'CTA', '2021-06-08 17:56:44', '2021-06-08 17:56:44');
INSERT INTO `dependencias` VALUES (6, 'COORDINACION DE SERVICIOS ESTUDIANTILES', 'CSU', '2021-06-08 17:57:56', '2021-06-08 17:57:56');
INSERT INTO `dependencias` VALUES (7, 'COORDINACION DE EGRESADOS', 'CEEX', '2021-06-08 17:58:15', '2021-06-08 17:58:15');
INSERT INTO `dependencias` VALUES (8, 'Coordinación General de Planeación y Evaluación', 'CGPE', '2021-06-08 18:00:12', '2021-06-08 18:00:12');
INSERT INTO `dependencias` VALUES (9, 'Coordinación General de Investigación, Posgrado y Vinculación', 'CGIP', '2021-06-08 18:00:47', '2021-06-08 18:00:47');
INSERT INTO `dependencias` VALUES (10, 'Coordinación General Académica y de Innovación', 'CGA', '2021-06-08 18:01:08', '2021-06-08 18:01:08');
COMMIT;

-- ----------------------------
-- Table structure for factura_archivos
-- ----------------------------
DROP TABLE IF EXISTS `factura_archivos`;
CREATE TABLE `factura_archivos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_solicitud` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_solicitud_factura_arch_fk` (`id_solicitud`),
  CONSTRAINT `id_solicitud_factura_arch_fk` FOREIGN KEY (`id_solicitud`) REFERENCES `solicitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of factura_archivos
-- ----------------------------
BEGIN;
INSERT INTO `factura_archivos` VALUES (1, 'Nueva Factura', '2021-05-28 21:44:25', '2021-06-08 19:31:50', 4);
INSERT INTO `factura_archivos` VALUES (3, 'ANEXO 1', '2021-06-08 19:32:04', '2021-06-08 19:32:04', 4);
INSERT INTO `factura_archivos` VALUES (4, 'ANEXO 2', '2021-06-08 19:32:10', '2021-06-08 19:32:10', 5);
INSERT INTO `factura_archivos` VALUES (5, 'ANEXO 3', '2021-06-08 19:32:19', '2021-06-08 19:32:19', 6);
INSERT INTO `factura_archivos` VALUES (6, 'ANEXO 4', '2021-06-08 19:32:27', '2021-06-08 19:32:27', 5);
INSERT INTO `factura_archivos` VALUES (7, 'ANEXO 5', '2021-06-08 19:32:32', '2021-06-08 19:32:32', 4);
COMMIT;

-- ----------------------------
-- Table structure for facturas
-- ----------------------------
DROP TABLE IF EXISTS `facturas`;
CREATE TABLE `facturas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_solicitud` bigint unsigned DEFAULT NULL,
  `folio_fiscal` varchar(50) DEFAULT NULL,
  `id_metodo` bigint unsigned DEFAULT NULL,
  `id_forma_pago` bigint unsigned DEFAULT NULL,
  `id_status` bigint unsigned DEFAULT NULL,
  `emisor` varchar(25) DEFAULT NULL,
  `total` decimal(12,2) DEFAULT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_solicitud_factura_fk` (`id_solicitud`),
  KEY `id_metodo_factura_fk` (`id_metodo`),
  KEY `id_forpago_factura_fk` (`id_forma_pago`),
  KEY `id_status_factura_fk` (`id_status`),
  CONSTRAINT `id_forpago_factura_fk` FOREIGN KEY (`id_forma_pago`) REFERENCES `forma_pagos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `id_metodo_factura_fk` FOREIGN KEY (`id_metodo`) REFERENCES `metodos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `id_solicitud_factura_fk` FOREIGN KEY (`id_solicitud`) REFERENCES `solicitudes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `id_status_factura_fk` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of facturas
-- ----------------------------
BEGIN;
INSERT INTO `facturas` VALUES (3, 4, '13131313', 7, 1, 2, '1', 1313.00, 'N/A', '2021-06-15 19:03:06', '2021-06-08 19:29:20');
INSERT INTO `facturas` VALUES (4, 5, '121212', 4, 4, 3, '2', 1212.00, 'N/A', '2021-06-15 19:03:22', '2021-06-08 19:30:07');
INSERT INTO `facturas` VALUES (6, 4, '141414', 6, 8, 5, '4', 1414.00, 'N/A', '2021-06-08 19:30:42', '2021-06-08 19:30:42');
COMMIT;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for fondos
-- ----------------------------
DROP TABLE IF EXISTS `fondos`;
CREATE TABLE `fondos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `numero` mediumint DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of fondos
-- ----------------------------
BEGIN;
INSERT INTO `fondos` VALUES (3, 1111, 'SUBSIDIO ORDINARIO GOBIERNO FEDERAL', '2021-06-08 18:06:21', '2021-05-06 05:15:42');
INSERT INTO `fondos` VALUES (5, 1112, 'SUBSIDIOS SEMS', '2021-06-08 18:07:14', '2021-05-06 05:15:56');
INSERT INTO `fondos` VALUES (8, 1115, 'SUBSIDIO ORDINARIO GOBIERNO ESTATAL', '2021-06-08 18:07:59', '2021-06-08 18:07:59');
INSERT INTO `fondos` VALUES (9, 1121, 'SERVICIOS ESCOLARES', '2021-06-08 18:08:57', '2021-06-08 18:08:57');
INSERT INTO `fondos` VALUES (11, 1122, 'INGRESOS PROPIOS', '2021-06-08 18:09:15', '2021-06-08 18:09:15');
INSERT INTO `fondos` VALUES (12, 1123, 'DONATIVOS', '2021-06-08 18:09:27', '2021-06-08 18:09:27');
INSERT INTO `fondos` VALUES (13, 1124, 'PRODUCTOS FINANCIEROS', '2021-06-08 18:09:36', '2021-06-08 18:09:36');
INSERT INTO `fondos` VALUES (14, 11313, 'PROMESAN', '2021-06-08 18:10:14', '2021-06-08 18:10:14');
INSERT INTO `fondos` VALUES (15, 11315, 'CONACULTA', '2021-06-08 18:10:26', '2021-06-08 18:10:26');
INSERT INTO `fondos` VALUES (16, 11318, 'SAGARPA', '2021-06-08 18:10:43', '2021-06-08 18:10:43');
COMMIT;

-- ----------------------------
-- Table structure for forma_pagos
-- ----------------------------
DROP TABLE IF EXISTS `forma_pagos`;
CREATE TABLE `forma_pagos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of forma_pagos
-- ----------------------------
BEGIN;
INSERT INTO `forma_pagos` VALUES (1, 'Efectivo', '2021-05-19 04:51:07', '2021-05-19 04:51:07');
INSERT INTO `forma_pagos` VALUES (2, 'Tarjeta', '2021-05-19 04:51:14', '2021-05-19 04:51:14');
INSERT INTO `forma_pagos` VALUES (3, 'Cheque Nominativo', '2021-06-08 18:34:16', '2021-06-08 18:34:16');
INSERT INTO `forma_pagos` VALUES (4, 'Transferencia Electrónica de Fondos SPEI', '2021-06-08 18:34:22', '2021-06-08 18:34:22');
INSERT INTO `forma_pagos` VALUES (5, 'Tarjeta de Crédito', '2021-06-08 18:34:28', '2021-06-08 18:34:28');
INSERT INTO `forma_pagos` VALUES (6, 'Monedero Electrónico', '2021-06-08 18:34:35', '2021-06-08 18:34:35');
INSERT INTO `forma_pagos` VALUES (7, 'Dinero Electrónico', '2021-06-08 18:34:41', '2021-06-08 18:34:41');
INSERT INTO `forma_pagos` VALUES (8, 'Vales de Despensa', '2021-06-08 18:34:46', '2021-06-08 18:34:46');
INSERT INTO `forma_pagos` VALUES (9, 'Dación en Pago', '2021-06-08 18:34:52', '2021-06-08 18:34:52');
INSERT INTO `forma_pagos` VALUES (10, 'Pago por Subrogación', '2021-06-08 18:34:58', '2021-06-08 18:34:58');
INSERT INTO `forma_pagos` VALUES (11, 'Pago por Consignación', '2021-06-08 18:35:03', '2021-06-08 18:35:03');
INSERT INTO `forma_pagos` VALUES (12, 'Condonación', '2021-06-08 18:35:08', '2021-06-08 18:35:08');
INSERT INTO `forma_pagos` VALUES (13, 'Compensación', '2021-06-08 18:35:12', '2021-06-08 18:35:12');
INSERT INTO `forma_pagos` VALUES (15, 'Confusión', '2021-06-08 18:35:20', '2021-06-08 18:35:20');
INSERT INTO `forma_pagos` VALUES (16, 'Remisión de Deuda', '2021-06-08 18:35:29', '2021-06-08 18:35:29');
INSERT INTO `forma_pagos` VALUES (17, 'Prescripción o Caducidad', '2021-06-08 18:35:35', '2021-06-08 18:35:35');
INSERT INTO `forma_pagos` VALUES (18, 'A Satisfacción del Acreedor', '2021-06-08 18:35:39', '2021-06-08 18:35:39');
INSERT INTO `forma_pagos` VALUES (19, 'Tarjeta de Débito', '2021-06-08 18:35:44', '2021-06-08 18:35:44');
INSERT INTO `forma_pagos` VALUES (20, 'Tarjeta de Servicios', '2021-06-08 18:35:52', '2021-06-08 18:35:52');
INSERT INTO `forma_pagos` VALUES (21, 'Aplicación de Anticipos', '2021-06-08 18:35:56', '2021-06-08 18:35:56');
COMMIT;

-- ----------------------------
-- Table structure for incidencias
-- ----------------------------
DROP TABLE IF EXISTS `incidencias`;
CREATE TABLE `incidencias` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of incidencias
-- ----------------------------
BEGIN;
INSERT INTO `incidencias` VALUES (1, 'Disturbios y alteración del orden público', '2021-05-19 04:51:54', '2021-06-08 18:41:56');
INSERT INTO `incidencias` VALUES (2, 'Accidente de trabajo', '2021-06-08 18:43:13', '2021-06-08 18:43:13');
INSERT INTO `incidencias` VALUES (3, 'Accidente de trayecto', '2021-06-08 18:43:18', '2021-06-08 18:43:18');
INSERT INTO `incidencias` VALUES (4, 'Días de castigo', '2021-06-08 18:43:26', '2021-06-08 18:43:26');
INSERT INTO `incidencias` VALUES (5, 'Horas extras', '2021-06-08 18:43:31', '2021-06-08 18:43:31');
INSERT INTO `incidencias` VALUES (6, 'Retardos', '2021-06-08 18:43:37', '2021-06-08 18:43:37');
INSERT INTO `incidencias` VALUES (7, 'Incapacidad', '2021-06-08 18:43:44', '2021-06-08 18:43:44');
INSERT INTO `incidencias` VALUES (8, 'Vacaciones pagadas', '2021-06-08 18:43:51', '2021-06-08 18:43:51');
COMMIT;

-- ----------------------------
-- Table structure for menu_items
-- ----------------------------
DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `order` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of menu_items
-- ----------------------------
BEGIN;
INSERT INTO `menu_items` VALUES (1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-04-13 20:03:30', '2021-04-13 20:03:30', 'voyager.dashboard', NULL);
INSERT INTO `menu_items` VALUES (2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 13, '2021-04-13 20:03:30', '2021-06-18 04:26:32', 'voyager.media.index', NULL);
INSERT INTO `menu_items` VALUES (3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 2, '2021-04-13 20:03:30', '2021-04-14 02:46:02', 'voyager.users.index', NULL);
INSERT INTO `menu_items` VALUES (4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 3, '2021-04-13 20:03:30', '2021-04-14 02:46:02', 'voyager.roles.index', NULL);
INSERT INTO `menu_items` VALUES (5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 11, '2021-04-13 20:03:30', '2021-06-18 04:26:36', NULL, NULL);
INSERT INTO `menu_items` VALUES (6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2021-04-13 20:03:30', '2021-04-14 02:45:14', 'voyager.menus.index', NULL);
INSERT INTO `menu_items` VALUES (7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2021-04-13 20:03:30', '2021-04-14 02:45:14', 'voyager.database.index', NULL);
INSERT INTO `menu_items` VALUES (8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2021-04-13 20:03:30', '2021-04-14 02:45:14', 'voyager.compass.index', NULL);
INSERT INTO `menu_items` VALUES (9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2021-04-13 20:03:30', '2021-04-14 02:45:14', 'voyager.bread.index', NULL);
INSERT INTO `menu_items` VALUES (10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 12, '2021-04-13 20:03:30', '2021-06-18 04:26:32', 'voyager.settings.index', NULL);
INSERT INTO `menu_items` VALUES (14, 1, 'Dependencias', '', '_self', 'voyager-company', NULL, NULL, 4, '2021-04-14 03:51:04', '2021-04-14 04:44:48', 'voyager.dependencias.index', NULL);
INSERT INTO `menu_items` VALUES (15, 1, 'Resp Dependencias', '', '_self', 'voyager-medal-rank-star', '#000000', NULL, 5, '2021-04-14 04:25:50', '2021-04-14 04:48:58', 'voyager.responsable-dependencias.index', 'null');
INSERT INTO `menu_items` VALUES (16, 1, 'Afin Tipos', '', '_self', 'voyager-paw', NULL, NULL, 6, '2021-06-15 18:44:12', '2021-06-15 18:44:36', 'voyager.afin-tipos.index', NULL);
INSERT INTO `menu_items` VALUES (19, 1, 'Bancos', '', '_self', 'voyager-treasure', NULL, NULL, 8, '2021-06-15 19:25:35', '2021-06-18 04:19:09', 'voyager.bancos.index', NULL);
INSERT INTO `menu_items` VALUES (20, 1, 'Status', '', '_self', 'voyager-news', '#000000', NULL, 7, '2021-06-15 19:42:50', '2021-06-18 04:23:12', 'voyager.status.index', 'null');
INSERT INTO `menu_items` VALUES (21, 1, 'Metodos', '', '_self', 'voyager-buy', '#000000', NULL, 10, '2021-06-18 04:18:54', '2021-06-18 04:26:36', 'voyager.metodos.index', 'null');
INSERT INTO `menu_items` VALUES (22, 1, 'Forma de pagos', '', '_self', 'voyager-dollar', '#000000', NULL, 9, '2021-06-18 04:25:50', '2021-06-18 04:26:22', 'voyager.forma-pagos.index', 'null');
COMMIT;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of menus
-- ----------------------------
BEGIN;
INSERT INTO `menus` VALUES (1, 'admin', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
COMMIT;

-- ----------------------------
-- Table structure for metodos
-- ----------------------------
DROP TABLE IF EXISTS `metodos`;
CREATE TABLE `metodos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of metodos
-- ----------------------------
BEGIN;
INSERT INTO `metodos` VALUES (2, 'Metodo 1', '2021-06-08 18:51:14', '2021-06-08 18:51:14');
INSERT INTO `metodos` VALUES (3, 'Metodo 2', '2021-06-08 18:51:16', '2021-06-08 18:51:16');
INSERT INTO `metodos` VALUES (4, 'Metodo 3', '2021-06-08 18:51:18', '2021-06-08 18:51:18');
INSERT INTO `metodos` VALUES (6, 'Metodo EDITADO DESDE VOY', '2021-06-08 18:51:00', '2021-06-15 18:53:24');
INSERT INTO `metodos` VALUES (7, 'METODO DESDE VOY', '2021-06-15 18:53:12', '2021-06-15 18:53:12');
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (93, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (94, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (95, '2016_01_01_000000_add_voyager_user_fields', 1);
INSERT INTO `migrations` VALUES (96, '2016_01_01_000000_create_data_types_table', 1);
INSERT INTO `migrations` VALUES (97, '2016_05_19_173453_create_menu_table', 1);
INSERT INTO `migrations` VALUES (98, '2016_10_21_190000_create_roles_table', 1);
INSERT INTO `migrations` VALUES (99, '2016_10_21_190000_create_settings_table', 1);
INSERT INTO `migrations` VALUES (100, '2016_11_30_135954_create_permission_table', 1);
INSERT INTO `migrations` VALUES (101, '2016_11_30_141208_create_permission_role_table', 1);
INSERT INTO `migrations` VALUES (102, '2016_12_26_201236_data_types__add__server_side', 1);
INSERT INTO `migrations` VALUES (103, '2017_01_13_000000_add_route_to_menu_items_table', 1);
INSERT INTO `migrations` VALUES (104, '2017_01_14_005015_create_translations_table', 1);
INSERT INTO `migrations` VALUES (105, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1);
INSERT INTO `migrations` VALUES (106, '2017_03_06_000000_add_controller_to_data_types_table', 1);
INSERT INTO `migrations` VALUES (107, '2017_04_21_000000_add_order_to_data_rows_table', 1);
INSERT INTO `migrations` VALUES (108, '2017_07_05_210000_add_policyname_to_data_types_table', 1);
INSERT INTO `migrations` VALUES (109, '2017_08_05_000000_add_group_to_settings_table', 1);
INSERT INTO `migrations` VALUES (110, '2017_11_26_013050_add_user_role_relationship', 1);
INSERT INTO `migrations` VALUES (111, '2017_11_26_015000_create_user_roles_table', 1);
INSERT INTO `migrations` VALUES (112, '2018_03_11_000000_add_user_settings', 1);
INSERT INTO `migrations` VALUES (113, '2018_03_14_000000_add_details_to_data_types_table', 1);
INSERT INTO `migrations` VALUES (114, '2018_03_16_000000_make_settings_value_nullable', 1);
INSERT INTO `migrations` VALUES (115, '2019_08_19_000000_create_failed_jobs_table', 1);
COMMIT;

-- ----------------------------
-- Table structure for movimientos
-- ----------------------------
DROP TABLE IF EXISTS `movimientos`;
CREATE TABLE `movimientos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  `tipo` varchar(25) DEFAULT NULL,
  `folio` varchar(15) DEFAULT NULL,
  `id_solicitud` bigint unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_solicitud_movimiento_kf` (`id_solicitud`),
  CONSTRAINT `id_solicitud_movimiento_kf` FOREIGN KEY (`id_solicitud`) REFERENCES `solicitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of movimientos
-- ----------------------------
BEGIN;
INSERT INTO `movimientos` VALUES (2, '2021-05-29 00:00:00', 'DESCRIPCIÓN DE EJEMPLO', 45.00, 'TRANSFERENCIA', '856', 4, '2021-06-08 18:52:09', '2021-05-28 20:11:22');
INSERT INTO `movimientos` VALUES (4, '2021-06-08 00:00:00', 'SIN DESCRIPCIÓN', 300000.00, 'Transferencia', 'T-0012345678', 1, '2021-06-08 18:52:25', '2021-06-08 18:30:13');
INSERT INTO `movimientos` VALUES (5, '2021-06-08 00:00:00', 'N/A', 130.00, 'PAGO', '1234', 1, '2021-06-08 19:25:36', '2021-06-08 19:25:36');
INSERT INTO `movimientos` VALUES (6, '2021-06-13 00:00:00', 'PAGO', 333.00, 'DEVOLUCIÓN', '4321', 4, '2021-06-08 19:26:03', '2021-06-08 19:26:03');
INSERT INTO `movimientos` VALUES (7, '2021-06-13 00:00:00', 'Transacción especial', 3333.00, 'Transacción', '1313', 6, '2021-06-08 19:26:53', '2021-06-08 19:26:53');
COMMIT;

-- ----------------------------
-- Table structure for paquete_solicitudes
-- ----------------------------
DROP TABLE IF EXISTS `paquete_solicitudes`;
CREATE TABLE `paquete_solicitudes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_paquete` bigint unsigned DEFAULT NULL,
  `id_solicitud` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paquete_solicitud_fk` (`id_paquete`),
  KEY `id_solicitud_paquete_fk` (`id_solicitud`),
  CONSTRAINT `id_paquete_solicitud_fk` FOREIGN KEY (`id_paquete`) REFERENCES `paquetes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_solicitud_paquete_fk` FOREIGN KEY (`id_solicitud`) REFERENCES `solicitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of paquete_solicitudes
-- ----------------------------
BEGIN;
INSERT INTO `paquete_solicitudes` VALUES (10, 5, 4, '2021-06-08 19:23:03', '2021-06-08 19:23:03');
INSERT INTO `paquete_solicitudes` VALUES (13, 5, 6, '2021-06-08 19:23:39', '2021-06-08 19:23:39');
INSERT INTO `paquete_solicitudes` VALUES (28, 1, 4, '2021-06-08 23:09:25', '2021-06-08 23:09:25');
INSERT INTO `paquete_solicitudes` VALUES (29, 5, 6, '2021-06-08 23:09:36', '2021-06-08 23:09:36');
INSERT INTO `paquete_solicitudes` VALUES (30, 13, 6, '2021-06-08 23:09:46', '2021-06-08 23:09:46');
INSERT INTO `paquete_solicitudes` VALUES (33, 13, 13, '2021-06-08 23:10:00', '2021-06-08 23:10:00');
INSERT INTO `paquete_solicitudes` VALUES (35, 6, 6, '2021-06-08 23:10:37', '2021-06-08 23:10:37');
INSERT INTO `paquete_solicitudes` VALUES (39, 1, 1, '2021-06-08 23:15:27', '2021-06-08 23:15:27');
INSERT INTO `paquete_solicitudes` VALUES (40, 1, 1, '2021-06-08 23:15:57', '2021-06-08 23:15:57');
INSERT INTO `paquete_solicitudes` VALUES (41, 5, 4, '2021-06-08 23:16:27', '2021-06-08 23:16:27');
COMMIT;

-- ----------------------------
-- Table structure for paquetes
-- ----------------------------
DROP TABLE IF EXISTS `paquetes`;
CREATE TABLE `paquetes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `numero` mediumint DEFAULT NULL,
  `solicitud_afin` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of paquetes
-- ----------------------------
BEGIN;
INSERT INTO `paquetes` VALUES (1, 45, '55', '2021-05-19 04:52:51', '2021-05-19 04:52:51');
INSERT INTO `paquetes` VALUES (2, 1, '1', '2021-06-08 19:21:59', '2021-06-08 19:21:59');
INSERT INTO `paquetes` VALUES (3, 2, '2', '2021-06-08 19:22:02', '2021-06-08 19:22:02');
INSERT INTO `paquetes` VALUES (4, 3, '3', '2021-06-08 19:22:05', '2021-06-08 19:22:05');
INSERT INTO `paquetes` VALUES (5, 4, '4', '2021-06-08 19:22:07', '2021-06-08 19:22:07');
INSERT INTO `paquetes` VALUES (6, 5, '5', '2021-06-08 19:22:14', '2021-06-08 19:22:14');
INSERT INTO `paquetes` VALUES (7, 5, '5', '2021-06-08 19:22:15', '2021-06-08 19:22:15');
INSERT INTO `paquetes` VALUES (8, 6, '6', '2021-06-08 19:22:20', '2021-06-08 19:22:20');
INSERT INTO `paquetes` VALUES (9, 7, '7', '2021-06-08 19:22:24', '2021-06-08 19:22:24');
INSERT INTO `paquetes` VALUES (10, 8, '8', '2021-06-08 19:22:26', '2021-06-08 19:22:26');
INSERT INTO `paquetes` VALUES (11, 9, '9', '2021-06-08 19:22:29', '2021-06-08 19:22:29');
INSERT INTO `paquetes` VALUES (12, 89, '48', '2021-06-08 23:05:33', '2021-06-08 23:05:33');
INSERT INTO `paquetes` VALUES (13, 45, '78', '2021-06-08 23:07:20', '2021-06-08 23:07:20');
INSERT INTO `paquetes` VALUES (14, 45, 'nueva', '2021-06-09 02:51:27', '2021-06-09 02:51:27');
COMMIT;

-- ----------------------------
-- Table structure for partidas
-- ----------------------------
DROP TABLE IF EXISTS `partidas`;
CREATE TABLE `partidas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_proyecto` bigint unsigned DEFAULT NULL,
  `numero` mediumint DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `cog` mediumint DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_proyecto_partida_fk` (`id_proyecto`),
  CONSTRAINT `id_proyecto_partida_fk` FOREIGN KEY (`id_proyecto`) REFERENCES `proyectos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of partidas
-- ----------------------------
BEGIN;
INSERT INTO `partidas` VALUES (6, 1, 12, 45.00, 'Nuevo', 45, '2021-05-18 04:11:36', '2021-05-18 03:42:48');
INSERT INTO `partidas` VALUES (7, 4, 8, 45.60, 'Hola', 784, '2021-06-08 18:55:30', '2021-05-28 00:37:16');
INSERT INTO `partidas` VALUES (8, 5, 112, 1231.00, 'N/A', 1230, '2021-06-08 18:55:54', '2021-06-08 18:55:54');
INSERT INTO `partidas` VALUES (9, 7, 432, 12321.00, 'N/A', 7653, '2021-06-08 18:56:08', '2021-06-08 18:56:08');
INSERT INTO `partidas` VALUES (10, 6, 43, 5463.00, 'N/A', 3123, '2021-06-08 18:56:25', '2021-06-08 18:56:25');
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
BEGIN;
INSERT INTO `permission_role` VALUES (1, 1);
INSERT INTO `permission_role` VALUES (2, 1);
INSERT INTO `permission_role` VALUES (3, 1);
INSERT INTO `permission_role` VALUES (4, 1);
INSERT INTO `permission_role` VALUES (5, 1);
INSERT INTO `permission_role` VALUES (6, 1);
INSERT INTO `permission_role` VALUES (7, 1);
INSERT INTO `permission_role` VALUES (8, 1);
INSERT INTO `permission_role` VALUES (9, 1);
INSERT INTO `permission_role` VALUES (10, 1);
INSERT INTO `permission_role` VALUES (11, 1);
INSERT INTO `permission_role` VALUES (12, 1);
INSERT INTO `permission_role` VALUES (13, 1);
INSERT INTO `permission_role` VALUES (14, 1);
INSERT INTO `permission_role` VALUES (15, 1);
INSERT INTO `permission_role` VALUES (16, 1);
INSERT INTO `permission_role` VALUES (17, 1);
INSERT INTO `permission_role` VALUES (18, 1);
INSERT INTO `permission_role` VALUES (19, 1);
INSERT INTO `permission_role` VALUES (20, 1);
INSERT INTO `permission_role` VALUES (21, 1);
INSERT INTO `permission_role` VALUES (22, 1);
INSERT INTO `permission_role` VALUES (23, 1);
INSERT INTO `permission_role` VALUES (24, 1);
INSERT INTO `permission_role` VALUES (25, 1);
INSERT INTO `permission_role` VALUES (41, 1);
INSERT INTO `permission_role` VALUES (42, 1);
INSERT INTO `permission_role` VALUES (43, 1);
INSERT INTO `permission_role` VALUES (44, 1);
INSERT INTO `permission_role` VALUES (45, 1);
INSERT INTO `permission_role` VALUES (46, 1);
INSERT INTO `permission_role` VALUES (47, 1);
INSERT INTO `permission_role` VALUES (48, 1);
INSERT INTO `permission_role` VALUES (49, 1);
INSERT INTO `permission_role` VALUES (50, 1);
INSERT INTO `permission_role` VALUES (51, 1);
INSERT INTO `permission_role` VALUES (52, 1);
INSERT INTO `permission_role` VALUES (53, 1);
INSERT INTO `permission_role` VALUES (54, 1);
INSERT INTO `permission_role` VALUES (55, 1);
INSERT INTO `permission_role` VALUES (56, 1);
INSERT INTO `permission_role` VALUES (57, 1);
INSERT INTO `permission_role` VALUES (58, 1);
INSERT INTO `permission_role` VALUES (59, 1);
INSERT INTO `permission_role` VALUES (60, 1);
INSERT INTO `permission_role` VALUES (61, 1);
INSERT INTO `permission_role` VALUES (62, 1);
INSERT INTO `permission_role` VALUES (63, 1);
INSERT INTO `permission_role` VALUES (64, 1);
INSERT INTO `permission_role` VALUES (65, 1);
INSERT INTO `permission_role` VALUES (66, 1);
INSERT INTO `permission_role` VALUES (67, 1);
INSERT INTO `permission_role` VALUES (68, 1);
INSERT INTO `permission_role` VALUES (69, 1);
INSERT INTO `permission_role` VALUES (70, 1);
INSERT INTO `permission_role` VALUES (71, 1);
INSERT INTO `permission_role` VALUES (72, 1);
INSERT INTO `permission_role` VALUES (73, 1);
INSERT INTO `permission_role` VALUES (74, 1);
INSERT INTO `permission_role` VALUES (75, 1);
COMMIT;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
BEGIN;
INSERT INTO `permissions` VALUES (1, 'browse_admin', NULL, '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (2, 'browse_bread', NULL, '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (3, 'browse_database', NULL, '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (4, 'browse_media', NULL, '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (5, 'browse_compass', NULL, '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (6, 'browse_menus', 'menus', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (7, 'read_menus', 'menus', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (8, 'edit_menus', 'menus', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (9, 'add_menus', 'menus', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (10, 'delete_menus', 'menus', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (11, 'browse_roles', 'roles', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (12, 'read_roles', 'roles', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (13, 'edit_roles', 'roles', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (14, 'add_roles', 'roles', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (15, 'delete_roles', 'roles', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (16, 'browse_users', 'users', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (17, 'read_users', 'users', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (18, 'edit_users', 'users', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (19, 'add_users', 'users', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (20, 'delete_users', 'users', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (21, 'browse_settings', 'settings', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (22, 'read_settings', 'settings', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (23, 'edit_settings', 'settings', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (24, 'add_settings', 'settings', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (25, 'delete_settings', 'settings', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `permissions` VALUES (41, 'browse_dependencias', 'dependencias', '2021-04-14 03:51:04', '2021-04-14 03:51:04');
INSERT INTO `permissions` VALUES (42, 'read_dependencias', 'dependencias', '2021-04-14 03:51:04', '2021-04-14 03:51:04');
INSERT INTO `permissions` VALUES (43, 'edit_dependencias', 'dependencias', '2021-04-14 03:51:04', '2021-04-14 03:51:04');
INSERT INTO `permissions` VALUES (44, 'add_dependencias', 'dependencias', '2021-04-14 03:51:04', '2021-04-14 03:51:04');
INSERT INTO `permissions` VALUES (45, 'delete_dependencias', 'dependencias', '2021-04-14 03:51:04', '2021-04-14 03:51:04');
INSERT INTO `permissions` VALUES (46, 'browse_responsable_dependencias', 'responsable_dependencias', '2021-04-14 04:25:50', '2021-04-14 04:25:50');
INSERT INTO `permissions` VALUES (47, 'read_responsable_dependencias', 'responsable_dependencias', '2021-04-14 04:25:50', '2021-04-14 04:25:50');
INSERT INTO `permissions` VALUES (48, 'edit_responsable_dependencias', 'responsable_dependencias', '2021-04-14 04:25:50', '2021-04-14 04:25:50');
INSERT INTO `permissions` VALUES (49, 'add_responsable_dependencias', 'responsable_dependencias', '2021-04-14 04:25:50', '2021-04-14 04:25:50');
INSERT INTO `permissions` VALUES (50, 'delete_responsable_dependencias', 'responsable_dependencias', '2021-04-14 04:25:50', '2021-04-14 04:25:50');
INSERT INTO `permissions` VALUES (51, 'browse_afin_tipos', 'afin_tipos', '2021-06-15 18:44:12', '2021-06-15 18:44:12');
INSERT INTO `permissions` VALUES (52, 'read_afin_tipos', 'afin_tipos', '2021-06-15 18:44:12', '2021-06-15 18:44:12');
INSERT INTO `permissions` VALUES (53, 'edit_afin_tipos', 'afin_tipos', '2021-06-15 18:44:12', '2021-06-15 18:44:12');
INSERT INTO `permissions` VALUES (54, 'add_afin_tipos', 'afin_tipos', '2021-06-15 18:44:12', '2021-06-15 18:44:12');
INSERT INTO `permissions` VALUES (55, 'delete_afin_tipos', 'afin_tipos', '2021-06-15 18:44:12', '2021-06-15 18:44:12');
INSERT INTO `permissions` VALUES (56, 'browse_metodos', 'metodos', '2021-06-15 18:52:36', '2021-06-15 18:52:36');
INSERT INTO `permissions` VALUES (57, 'read_metodos', 'metodos', '2021-06-15 18:52:36', '2021-06-15 18:52:36');
INSERT INTO `permissions` VALUES (58, 'edit_metodos', 'metodos', '2021-06-15 18:52:36', '2021-06-15 18:52:36');
INSERT INTO `permissions` VALUES (59, 'add_metodos', 'metodos', '2021-06-15 18:52:36', '2021-06-15 18:52:36');
INSERT INTO `permissions` VALUES (60, 'delete_metodos', 'metodos', '2021-06-15 18:52:36', '2021-06-15 18:52:36');
INSERT INTO `permissions` VALUES (61, 'browse_forma_pagos', 'forma_pagos', '2021-06-15 19:01:15', '2021-06-15 19:01:15');
INSERT INTO `permissions` VALUES (62, 'read_forma_pagos', 'forma_pagos', '2021-06-15 19:01:15', '2021-06-15 19:01:15');
INSERT INTO `permissions` VALUES (63, 'edit_forma_pagos', 'forma_pagos', '2021-06-15 19:01:15', '2021-06-15 19:01:15');
INSERT INTO `permissions` VALUES (64, 'add_forma_pagos', 'forma_pagos', '2021-06-15 19:01:15', '2021-06-15 19:01:15');
INSERT INTO `permissions` VALUES (65, 'delete_forma_pagos', 'forma_pagos', '2021-06-15 19:01:15', '2021-06-15 19:01:15');
INSERT INTO `permissions` VALUES (66, 'browse_bancos', 'bancos', '2021-06-15 19:25:35', '2021-06-15 19:25:35');
INSERT INTO `permissions` VALUES (67, 'read_bancos', 'bancos', '2021-06-15 19:25:35', '2021-06-15 19:25:35');
INSERT INTO `permissions` VALUES (68, 'edit_bancos', 'bancos', '2021-06-15 19:25:35', '2021-06-15 19:25:35');
INSERT INTO `permissions` VALUES (69, 'add_bancos', 'bancos', '2021-06-15 19:25:35', '2021-06-15 19:25:35');
INSERT INTO `permissions` VALUES (70, 'delete_bancos', 'bancos', '2021-06-15 19:25:35', '2021-06-15 19:25:35');
INSERT INTO `permissions` VALUES (71, 'browse_status', 'status', '2021-06-15 19:42:50', '2021-06-15 19:42:50');
INSERT INTO `permissions` VALUES (72, 'read_status', 'status', '2021-06-15 19:42:50', '2021-06-15 19:42:50');
INSERT INTO `permissions` VALUES (73, 'edit_status', 'status', '2021-06-15 19:42:50', '2021-06-15 19:42:50');
INSERT INTO `permissions` VALUES (74, 'add_status', 'status', '2021-06-15 19:42:50', '2021-06-15 19:42:50');
INSERT INTO `permissions` VALUES (75, 'delete_status', 'status', '2021-06-15 19:42:50', '2021-06-15 19:42:50');
COMMIT;

-- ----------------------------
-- Table structure for proveedores
-- ----------------------------
DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE `proveedores` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(100) DEFAULT NULL,
  `rfc` varchar(15) DEFAULT NULL,
  `cuenta` varchar(25) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `id_banco` bigint unsigned DEFAULT NULL,
  `clabe` varchar(18) CHARACTER SET utf16 COLLATE utf16_general_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_banco_proveedor` (`id_banco`),
  CONSTRAINT `id_banco_proveedor` FOREIGN KEY (`id_banco`) REFERENCES `bancos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of proveedores
-- ----------------------------
BEGIN;
INSERT INTO `proveedores` VALUES (1, 'hp computers', 'FOBP785846JH9', '21454785', 'C 5 sur 1423', 'hp@computers.com', '3356897845', 'Ninguna', 3, '45111421212', '2021-05-19 04:54:43', '2021-05-19 04:54:43');
INSERT INTO `proveedores` VALUES (2, 'Dell Guadalajra', 'DGDL457854JI9', '457854', 'Lazaro Cardenas 21', 'dell_gdl@mail.com', '3345789645', 'Ninguna', 4, '457898564', '2021-05-19 05:04:12', '2021-05-19 05:03:28');
INSERT INTO `proveedores` VALUES (3, 'Compañía Periodística Nacional, S.A.', 'PNA-861121', '123123123', 'Dirección de prueba', 'cpn@gmail.com', '3316673823', 'N/A', 11, '123123123123123', '2021-06-08 19:02:10', '2021-06-08 19:02:10');
INSERT INTO `proveedores` VALUES (4, 'Compañía Periodística Nacional, S.A.', 'PNA-861121', '123123123', 'Dirección de prueba', 'cpn@gmail.com', '3316673823', 'N/A', 11, '123123123123123', '2021-06-08 19:02:11', '2021-06-08 19:02:11');
INSERT INTO `proveedores` VALUES (5, 'Cía. De Artículos Eléctricos, S. de R.L.', 'AEL-850110', '345675567', 'Dirección de prueba', 'caelec@gmail.com', '54364312311', 'N/A', 10, '23423453234', '2021-06-08 19:02:49', '2021-06-08 19:02:49');
INSERT INTO `proveedores` VALUES (6, 'Pimienta Hnos. y Cía., S.A.', 'PHN-830228', '64575872', 'Dirección de prueba', 'pimientahnos@gmail.com', '3345672312', 'N/A', 9, '86752345122', '2021-06-08 19:03:23', '2021-06-08 19:03:23');
INSERT INTO `proveedores` VALUES (7, 'Sociedad de Producción Rural de Sonora', 'PRS-800101', '23434534', 'Dirección de prueba', 'soproso@gmail.com', '4423323425', 'N/A', 8, '5667578947654', '2021-06-08 19:03:54', '2021-06-08 19:03:54');
COMMIT;

-- ----------------------------
-- Table structure for proyectos
-- ----------------------------
DROP TABLE IF EXISTS `proyectos`;
CREATE TABLE `proyectos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `numero` mediumint DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `id_fondo` bigint unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_fondo_proyecto_fk` (`id_fondo`),
  CONSTRAINT `id_fondo_proyecto_fk` FOREIGN KEY (`id_fondo`) REFERENCES `fondos` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of proyectos
-- ----------------------------
BEGIN;
INSERT INTO `proyectos` VALUES (1, 4578, 'INE', 3, '2021-05-18 03:16:55', '2021-05-18 03:16:55');
INSERT INTO `proyectos` VALUES (2, 7887, 'CGSI', 5, '2021-05-18 03:17:13', '2021-05-18 03:17:13');
INSERT INTO `proyectos` VALUES (3, 256235, 'GASTOS DE OPERACIÓN DE LA COORDINACIÓN GENERAL DE SERVICIOS A UNIVERSITARIOS', 9, '2021-06-08 18:20:11', '2021-06-08 18:20:11');
INSERT INTO `proyectos` VALUES (4, 256235, 'SIIE Patrimonio', 9, '2021-06-08 18:21:12', '2021-06-08 18:21:12');
INSERT INTO `proyectos` VALUES (5, 156235, 'Migración Exchange', 16, '2021-06-08 18:21:29', '2021-06-08 18:21:29');
INSERT INTO `proyectos` VALUES (6, 153215, 'Repositorio Documental CGTI', 9, '2021-06-08 18:21:44', '2021-06-08 18:21:44');
INSERT INTO `proyectos` VALUES (7, 3215, 'Monitoreo de Sistemas', 13, '2021-06-08 18:21:57', '2021-06-08 18:21:57');
COMMIT;

-- ----------------------------
-- Table structure for reintegros
-- ----------------------------
DROP TABLE IF EXISTS `reintegros`;
CREATE TABLE `reintegros` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `folio` mediumint DEFAULT NULL,
  `id_solicitud` bigint unsigned DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `referencia` mediumint DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_solicitud_reintegro_fk` (`id_solicitud`),
  CONSTRAINT `id_solicitud_reintegro_fk` FOREIGN KEY (`id_solicitud`) REFERENCES `solicitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of reintegros
-- ----------------------------
BEGIN;
INSERT INTO `reintegros` VALUES (1, 54, 1, '2021-05-28 00:00:00', '87', 45540, 87874843.00, 'Ninguna otra', '2021-05-28 20:27:17', '2021-05-28 20:22:47');
INSERT INTO `reintegros` VALUES (2, 111, 4, '2021-06-08 00:00:00', 'Reintegro por donación', 111, 111.00, 'N/A', '2021-06-08 19:19:03', '2021-06-08 19:19:03');
INSERT INTO `reintegros` VALUES (3, 222, 6, '2021-06-13 00:00:00', 'Reintegro de emergencia', 222, 222.00, 'N/A', '2021-06-08 19:19:31', '2021-06-08 19:19:31');
INSERT INTO `reintegros` VALUES (4, 133, 6, '2021-06-30 00:00:00', 'Reintegro de prueba', 133, 133.00, 'N/A', '2021-06-08 19:19:52', '2021-06-08 19:19:52');
INSERT INTO `reintegros` VALUES (5, 421, 5, '2021-06-30 00:00:00', 'Reintegro de daños', 421, 421.00, 'N/A', '2021-06-08 19:20:16', '2021-06-08 19:20:16');
COMMIT;

-- ----------------------------
-- Table structure for responsable_dependencias
-- ----------------------------
DROP TABLE IF EXISTS `responsable_dependencias`;
CREATE TABLE `responsable_dependencias` (
  `id_user` bigint unsigned DEFAULT NULL,
  `id_dependencia` tinyint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `responsable_dependencias_id_user_unique` (`id_user`),
  UNIQUE KEY `responsable_dependencias_id_dependencia_unique` (`id_dependencia`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of responsable_dependencias
-- ----------------------------
BEGIN;
INSERT INTO `responsable_dependencias` VALUES (1, 1, '2021-04-14 04:32:34', '2021-04-14 04:32:34', 1);
INSERT INTO `responsable_dependencias` VALUES (2, 2, '2021-04-14 04:40:57', '2021-04-14 04:40:57', 4);
INSERT INTO `responsable_dependencias` VALUES (3, 4, '2021-05-19 23:11:31', '2021-05-19 23:11:31', 7);
COMMIT;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES (1, 'admin', 'Administrator', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
INSERT INTO `roles` VALUES (2, 'user', 'Normal User', '2021-04-13 20:03:30', '2021-04-13 20:03:30');
COMMIT;

-- ----------------------------
-- Table structure for saldo_partidas
-- ----------------------------
DROP TABLE IF EXISTS `saldo_partidas`;
CREATE TABLE `saldo_partidas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_fondo` bigint unsigned DEFAULT NULL,
  `id_partida` bigint unsigned DEFAULT NULL,
  `inicial` decimal(12,2) DEFAULT NULL,
  `adeudo` decimal(12,2) DEFAULT NULL,
  `capturado` decimal(12,2) DEFAULT NULL,
  `solicitado` decimal(12,2) DEFAULT NULL,
  `pagado` decimal(12,2) DEFAULT NULL,
  `compensado` decimal(12,2) DEFAULT NULL,
  `porcentaje_compensado` decimal(5,2) DEFAULT NULL,
  `saldo` decimal(12,2) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_fondo_saldopar_fk` (`id_fondo`),
  KEY `id_partida_saldopar_fk` (`id_partida`),
  CONSTRAINT `id_fondo_saldopar_fk` FOREIGN KEY (`id_fondo`) REFERENCES `fondos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_partida_saldopar_fk` FOREIGN KEY (`id_partida`) REFERENCES `partidas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of saldo_partidas
-- ----------------------------
BEGIN;
INSERT INTO `saldo_partidas` VALUES (2, 5, 7, 12.00, 21.00, 12.00, 12.00, 21.00, 21.00, 121.00, 221.00, '2021-06-08 22:48:04', '2021-05-28 20:39:45');
INSERT INTO `saldo_partidas` VALUES (3, 3, 10, 123.00, 1230.00, 123.00, 123.00, 231.00, 231.00, 123.00, 123.00, '2021-06-08 18:57:10', '2021-06-08 18:57:10');
INSERT INTO `saldo_partidas` VALUES (4, 5, 7, 312.00, 21.00, 213.00, 312.00, 231321.00, 312.00, 123.00, 312.00, '2021-06-08 18:57:28', '2021-06-08 18:57:28');
INSERT INTO `saldo_partidas` VALUES (5, 14, 10, 31245.00, 2156.00, 21345.00, 31254.00, 23132.00, 31245.00, 456.00, 31245.00, '2021-06-08 18:58:40', '2021-06-08 18:58:40');
COMMIT;

-- ----------------------------
-- Table structure for saldo_proyectos
-- ----------------------------
DROP TABLE IF EXISTS `saldo_proyectos`;
CREATE TABLE `saldo_proyectos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_fondo` bigint unsigned DEFAULT NULL,
  `id_proyecto` bigint unsigned DEFAULT NULL,
  `inicial` decimal(12,2) DEFAULT NULL,
  `capturado` decimal(12,2) DEFAULT NULL,
  `solicitado` decimal(12,2) DEFAULT NULL,
  `pagado` decimal(12,2) DEFAULT NULL,
  `compensado` decimal(12,2) DEFAULT NULL,
  `porcentaje_compensado` decimal(5,2) DEFAULT NULL,
  `saldo` decimal(12,2) DEFAULT NULL,
  `comprobado` decimal(12,2) DEFAULT NULL,
  `adeudo` decimal(12,2) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_fondo_saldoproy_fk` (`id_fondo`),
  KEY `id_proyecto_saldoproy_fk` (`id_proyecto`),
  CONSTRAINT `id_fondo_saldoproy_fk` FOREIGN KEY (`id_fondo`) REFERENCES `fondos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_proyecto_saldoproy_fk` FOREIGN KEY (`id_proyecto`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of saldo_proyectos
-- ----------------------------
BEGIN;
INSERT INTO `saldo_proyectos` VALUES (1, 5, 1, 12.30, 32.10, 45.60, 78.10, 45.50, 80.60, 250.00, 100.00, 150.00, '2021-05-24 20:08:58', '2021-05-24 20:09:02');
INSERT INTO `saldo_proyectos` VALUES (3, 3, 2, 45.33, 78.25, 45.33, 45.77, 78.55, 54.30, 11.00, 45.70, 99.56, '2021-05-25 02:47:28', '2021-05-25 02:46:39');
INSERT INTO `saldo_proyectos` VALUES (4, 3, 4, 333.00, 111.00, 111.00, 222.00, 333.00, 11.00, 133.00, 131.00, 131.00, '2021-06-08 18:54:03', '2021-06-08 18:54:03');
INSERT INTO `saldo_proyectos` VALUES (5, 12, 2, 222.00, 444.00, 111.00, 44.00, 124.00, 12.00, 421.00, 1124.00, 234.00, '2021-06-08 18:54:29', '2021-06-08 18:54:29');
INSERT INTO `saldo_proyectos` VALUES (6, 14, 5, 3412.00, 42134.00, 43134.00, 44424.00, 124.00, 124.00, 512345.00, 5212.00, 2344312.00, '2021-06-08 18:54:58', '2021-06-08 18:54:58');
COMMIT;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NOT NULL DEFAULT '1',
  `group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
BEGIN;
INSERT INTO `settings` VALUES (1, 'site.title', 'Site Title', 'SUBILLS', '', 'text', 1, 'Site');
INSERT INTO `settings` VALUES (2, 'site.description', 'Site Description', 'Aplicacion Web para la gestion de licitaciones, egresos e ingresos del departamento de Finanzas de la Coordinación General de Servicios UniversitariosSite Description', '', 'text', 2, 'Site');
INSERT INTO `settings` VALUES (3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site');
INSERT INTO `settings` VALUES (4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site');
INSERT INTO `settings` VALUES (5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin');
INSERT INTO `settings` VALUES (6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin');
INSERT INTO `settings` VALUES (7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin');
INSERT INTO `settings` VALUES (8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin');
INSERT INTO `settings` VALUES (9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin');
INSERT INTO `settings` VALUES (10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');
COMMIT;

-- ----------------------------
-- Table structure for solicitudes
-- ----------------------------
DROP TABLE IF EXISTS `solicitudes`;
CREATE TABLE `solicitudes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_status` bigint unsigned DEFAULT NULL,
  `id_proyecto` bigint unsigned DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `folio` mediumint unsigned DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `id_beneficiario` bigint unsigned DEFAULT NULL,
  `tipo` bigint DEFAULT NULL,
  `tipo_afin` bigint DEFAULT NULL,
  `id_fondo` bigint unsigned DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  `reintegro` decimal(12,2) unsigned DEFAULT NULL,
  `orden_compra` varchar(25) DEFAULT NULL,
  `solicitud_afin` bigint unsigned DEFAULT NULL,
  `aprobacion_afin` bigint unsigned DEFAULT NULL,
  `orden_compra_afin` bigint unsigned DEFAULT NULL,
  `recepcion_afin` bigint unsigned DEFAULT NULL,
  `factura_afin` bigint DEFAULT NULL,
  `movimiento` varchar(15) DEFAULT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `paquete_finanzas` bigint DEFAULT NULL,
  `recibido_finanzas` datetime DEFAULT NULL,
  `id_incidencia` bigint unsigned DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `id_usuario_solicitante` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `id_status_solicitud_fk` (`id_status`),
  KEY `id_proyecto_solicitud_fk` (`id_proyecto`),
  KEY `id_fondo_solicitud_fk` (`id_fondo`),
  KEY `id_beneficiario_solicitud_fk` (`id_beneficiario`),
  KEY `id_usuariosoli_solicitud_fk` (`id_usuario_solicitante`),
  KEY `id_incidencia_solicitud_fk` (`id_incidencia`),
  CONSTRAINT `id_beneficiario_solicitud_fk` FOREIGN KEY (`id_beneficiario`) REFERENCES `beneficiarios` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `id_fondo_solicitud_fk` FOREIGN KEY (`id_fondo`) REFERENCES `fondos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `id_incidencia_solicitud_fk` FOREIGN KEY (`id_incidencia`) REFERENCES `incidencias` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `id_proyecto_solicitud_fk` FOREIGN KEY (`id_proyecto`) REFERENCES `proyectos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `id_status_solicitud_fk` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `id_usuariosoli_solicitud_fk` FOREIGN KEY (`id_usuario_solicitante`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of solicitudes
-- ----------------------------
BEGIN;
INSERT INTO `solicitudes` VALUES (1, 1, 2, '2021-05-20 00:00:00', 101, 'Solicitud de prueba', 1, 12, 42, 5, 245.00, 322.00, '45421', 454554, 5325, 123465, 45154, 56232565, 'Un movimiento', '2021-05-12 00:00:00', 444545, '2021-05-20 00:00:00', 1, 'Ninguna', 1, '2021-05-20 01:04:10', '2021-06-08 19:11:09');
INSERT INTO `solicitudes` VALUES (4, 1, 2, '2021-12-31 00:00:00', 371, 'Subsidio CTA', 1, 13, 13, 8, 333.00, 555.00, '131313', 13, 13, 13, 13, 13, 'Nuevo Mov 13', '2021-05-07 00:00:00', 13, '2019-02-02 00:00:00', 1, 'Muchas observaciones jejejeje.', 2, '2021-05-21 04:08:12', '2021-06-15 18:22:16');
INSERT INTO `solicitudes` VALUES (5, 1, 3, '2021-06-08 00:00:00', 103, 'Solicitud de gastos de operación', 1, 1, 1, 3, 1.00, 1.00, '1', 1, 1, 1, 1, 1, 'N/A', '2021-06-08 00:00:00', 1, '2021-06-08 00:00:00', 1, 'N/A', 1, '2021-06-08 19:10:48', '2021-06-08 19:13:08');
INSERT INTO `solicitudes` VALUES (6, 4, 6, '2021-06-26 00:00:00', 133, 'Solicitud de donativos', 2, 2, 2, 12, 2.00, 20.00, '2', 2, 2, 2, 2, 2, 'N/A', '2021-06-04 00:00:00', 2, '2021-06-08 00:00:00', 2, 'N/A', 2, '2021-06-08 19:12:39', '2021-06-08 19:13:36');
INSERT INTO `solicitudes` VALUES (13, 2, 7, '2021-06-08 00:00:00', 303, 'Apoyo monitoreo de Sistemas', 4, 1, 3, 14, 3.30, 4.00, '4', 4, 4, 4, 4, 4, 'N/A', '2021-06-08 00:00:00', 4, '2021-06-20 00:00:00', 1, 'N/A', 1, '2021-06-08 19:18:07', '2021-06-08 19:18:07');
COMMIT;

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `concepto` varchar(15) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of status
-- ----------------------------
BEGIN;
INSERT INTO `status` VALUES (1, 'EDICION', '2021-06-08 18:45:14', '2021-05-19 04:49:32');
INSERT INTO `status` VALUES (2, 'REVISIÓN', '2021-06-08 18:45:24', '2021-06-08 18:45:24');
INSERT INTO `status` VALUES (3, 'APROBADA', '2021-06-08 18:45:28', '2021-06-08 18:45:28');
INSERT INTO `status` VALUES (4, 'RECHAZADA', '2021-06-08 18:45:33', '2021-06-08 18:45:33');
INSERT INTO `status` VALUES (5, 'CANCELADA', '2021-06-08 18:45:38', '2021-06-08 18:45:38');
COMMIT;

-- ----------------------------
-- Table structure for translations
-- ----------------------------
DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int unsigned NOT NULL,
  `locale` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of translations
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint unsigned DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_dependencia` tinyint unsigned DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  KEY `users_dependencia_index` (`id_dependencia`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 1, 'paris@admin.com', 'users/default.png', '$2y$10$gBIV6BR/1Q9plUHjHWYPteC3YhLRVry6vRZ.adCWK.dchZgxpwFJm', '2ZlU3phrGvBtVYqLgOujK0Ggeff1H5DA8qRAnKLBY4e3qChJQBZip791fWO9', '{\"locale\":\"en\"}', 'Paris Flores Belmares', '3312295568', 2, NULL, '2021-04-13 20:04:50', '2021-04-14 04:19:24');
INSERT INTO `users` VALUES (2, 2, 'atzielin@gmail.com', 'users/default.png', '$2y$10$fW9XGrmmV8IRrIkd3OHdBefRmbKBgaa0i6fFSlq0O5VBPHk3JqILO', NULL, '{\"locale\":\"es\"}', 'Natzieli M Conde', '3317276717', 1, NULL, '2021-04-14 01:03:59', '2021-04-14 04:13:50');
INSERT INTO `users` VALUES (3, 1, 'pgb@mail.com', 'users/default.png', '$2y$10$ky27pph0DVNk3D4Bzr7.p.NTFchTMKfJEf3OHowDl0pbmvCApbp1y', NULL, '{\"locale\":\"en\"}', 'Paris', '639', 1, NULL, '2021-05-19 23:10:45', '2021-05-19 23:10:45');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
